import View.ClientView;
import Controller.Controller;

import java.awt.*;

public class MainApp {
	public static void main(String[] args){
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Controller(new ClientView());
			}
		});
	}
}
