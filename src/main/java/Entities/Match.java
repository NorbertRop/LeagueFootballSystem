package Entities;

import java.sql.Date;

public class Match {
	private int matchID;
	private String team1;
	private String team2;
	private String homeTeam;
	private Date MatchTime;
	private String league;
	private int shootsTeam1 = -1;
	private int shootsTeam2 = -1;
	private int possessionTeam1= -1;
	private int possessionTeam2 = -1;
	private int POTM = -1;
	private int Round = -1;

	public int getMatchID() {
		return matchID;
	}

	public void setMatchID(int matchID) {
		this.matchID = matchID;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public int getShootsTeam1() {
		return shootsTeam1;
	}

	public void setShootsTeam1(int shootsTeam1) {
		this.shootsTeam1 = shootsTeam1;
	}

	public int getShootsTeam2() {
		return shootsTeam2;
	}

	public void setShootsTeam2(int shootsTeam2) {
		this.shootsTeam2 = shootsTeam2;
	}

	public int getPossessionTeam1() {
		return possessionTeam1;
	}

	public void setPossessionTeam1(int possessionTeam1) {
		this.possessionTeam1 = possessionTeam1;
	}

	public int getPossessionTeam2() {
		return possessionTeam2;
	}

	public void setPossessionTeam2(int possessionTeam2) {
		this.possessionTeam2 = possessionTeam2;
	}

	public int getPOTM() {
		return POTM;
	}

	public void setPOTM(int POTM) {
		this.POTM = POTM;
	}

	public int getRound() {
		return Round;
	}

	public void setRound(int round) {
		Round = round;
	}

	public Date getMatchTime() {
		return MatchTime;
	}

	public void setMatchTime(Date matchTime) {
		MatchTime = matchTime;
	}
}
