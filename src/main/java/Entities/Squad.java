package Entities;

public class Squad {
	private int footballer = -1;
	private int match = -1;
	private Boolean startingEleven = null;

	public int getFootballer() {
		return footballer;
	}

	public void setFootballer(int footballer) {
		this.footballer = footballer;
	}

	public int getMatch() {
		return match;
	}

	public void setMatch(int match) {
		this.match = match;
	}

	public Boolean isStartingEleven() {
		return startingEleven;
	}

	public void setStartingEleven(Boolean startingEleven) {
		this.startingEleven = startingEleven;
	}
}
