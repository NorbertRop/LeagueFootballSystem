package Entities;

public class GoalAssist {
	private int goalID;
	private int Footballer = -1;
	private String type;
	private int match = -1;

	public int getGoalID() {
		return goalID;
	}

	public void setGoalID(int goalID) {
		this.goalID = goalID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMatch() {
		return match;
	}

	public void setMatch(int match) {
		this.match = match;
	}

	public int getFootballer() {
		return Footballer;
	}

	public void setFootballer(int footballer) {
		Footballer = footballer;
	}
}
