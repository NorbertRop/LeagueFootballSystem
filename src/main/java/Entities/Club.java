package Entities;

public class Club {
	private String fullName;
	private String stadium;
	private int points;
	private int goalsBalance;
	private int wins;
	private int losses;
	private int draws;
	private String league;

	public String getFullName() {
		return fullName;
	}

	public String getStadium() {
		return stadium;
	}

	public int getPoints() {
		return points;
	}

	public int getGoalsBalance() {
		return goalsBalance;
	}

	public int getWins() {
		return wins;
	}

	public int getLosses() {
		return losses;
	}

	public int getDraws() {
		return draws;
	}

	public String getLeague() {
		return league;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setStadium(String stadium) {
		this.stadium = stadium;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public void setGoalsBalance(int goalsBalance) {
		this.goalsBalance = goalsBalance;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public void setLosses(int losses) {
		this.losses = losses;
	}

	public void setDraws(int draws) {
		this.draws = draws;
	}

	public void setLeague(String league) {
		this.league = league;
	}
}
