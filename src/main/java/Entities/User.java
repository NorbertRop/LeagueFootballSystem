package Entities;

public class User {
	private String nick;
	private byte[] hashPass;
	private byte[] salt;
	private String type;
	private String favTeam;

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public byte[] getPass() {
		return hashPass;
	}

	public void setPass(byte[] pass) {
		this.hashPass = pass;
	}

	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public String getFavTeam() {
		return favTeam;
	}

	public void setFavTeam(String favTeam) {
		this.favTeam = favTeam;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
