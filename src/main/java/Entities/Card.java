package Entities;

public class Card {
	private int cardID;
	private int footballer = -1;
	private String Type;
	private int match = -1;

	public int getCardID() {
		return cardID;
	}

	public void setCardID(int cardID) {
		this.cardID = cardID;
	}

	public int getFootballer() {
		return footballer;
	}

	public void setFootballer(int footballer) {
		this.footballer = footballer;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public int getMatch() {
		return match;
	}

	public void setMatch(int match) {
		this.match = match;
	}
}
