package DAO;

import Entities.Match;

import java.sql.Date;
import java.util.ArrayList;

public interface MatchDAO {
	void addMatch(Match match);
	void removeMatch(Match match);
	void updateMatch(Match updatedMatch, Match insertedMatch);
	ArrayList<Match> getAllMatches();
	ArrayList<Match> getMatches(Match match);
	Date getNextMatch(String team);
	ArrayList<Match> getRound(int round);
}
