package DAO;

import Entities.Footballer;

import java.util.ArrayList;

public interface FootballerDAO {
	void addFootballer(Footballer footballer);
	void removeFootballer(Footballer footballer);
	void updateFootballer(Footballer footballerUpdated, Footballer footballerInserted);
	ArrayList<Footballer> getAllFootballers();
	ArrayList<Footballer> getFootballers(Footballer footballer);
	ArrayList<Footballer> getGoalKing();
	ArrayList<Footballer> getAssistsKing();
}
