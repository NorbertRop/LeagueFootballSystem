package DAO;

import Entities.Card;

import java.util.ArrayList;

public interface CardDAO {
	void addCard(Card card);
	void removeCard(Card card);
	ArrayList<Card> getCards(Card card);
}
