package DAO;

import DBUtils.DBConnection;
import Entities.Stadium;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class StadiumDAOImp implements StadiumDAO {
	private Connection connection;

	public StadiumDAOImp() {
		connection = DBConnection.getConnection();
	}

	@Override
	public void addStadium(Stadium stadium) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
						"stadiums(Name, Capacity, City) VALUES (?, ?, ?)");
				preparedStatement.setString(1, stadium.getName());
				preparedStatement.setInt(2, stadium.getCapacity());
				preparedStatement.setString(3, stadium.getCity());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeStadium(Stadium stadium) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM stadiums " +
						"WHERE Name = ? AND Capacity = ? AND City = ?");
				preparedStatement.setString(1, stadium.getName());
				preparedStatement.setInt(2, stadium.getCapacity());
				preparedStatement.setString(3, stadium.getCity());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void updateStadium(Stadium updatedStadium, Stadium insertedStadium) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("UPDATE stadiums " +
						"SET Name = ?, Capacity = ?, City = ? WHERE Name = ? AND Capacity = ? AND City = ?");
				preparedStatement.setString(1, insertedStadium.getName());
				preparedStatement.setInt(2, insertedStadium.getCapacity());
				preparedStatement.setString(3, insertedStadium.getCity());
				preparedStatement.setString(4, updatedStadium.getName());
				preparedStatement.setInt(5, updatedStadium.getCapacity());
				preparedStatement.setString(6, updatedStadium.getCity());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<Stadium> getAllStadiums() {
		ArrayList<Stadium> stadiumList = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM stadiums");
			fillList(stadiumList, resultSet);
		} catch (SQLException ex) {
		}

		return stadiumList;
	}

	public ArrayList<Stadium> getStadiums(Stadium stadium) {
		ArrayList<Stadium> stadiumList = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM stadiums WHERE name LIKE ? AND capacity LIKE ? "
					+ "AND city LIKE ?");
			if (!(stadium.getName() == null)) {
				preparedStatement.setString(1, "%" + stadium.getName() + "%");
			} else {
				preparedStatement.setString(1, "%");
			}
			if (!(stadium.getCapacity() == -1)) {
				preparedStatement.setString(2, "%" + stadium.getCapacity() + "%");
			} else {
				preparedStatement.setString(2, "%");
			}
			if (!(stadium.getCity() == null)) {
				preparedStatement.setString(3, "%" + stadium.getCity() + "%");
			} else {
				preparedStatement.setString(3, "%");
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			fillList(stadiumList, resultSet);
		} catch (SQLException ex) {
		}

		return stadiumList;
	}

	private void fillList(ArrayList<Stadium> stadiumList, ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			Stadium s = new Stadium();
			s.setName(resultSet.getString("Name"));
			s.setCapacity(resultSet.getInt("Capacity"));
			s.setCity(resultSet.getString("City"));
			stadiumList.add(s);
		}
	}

}
