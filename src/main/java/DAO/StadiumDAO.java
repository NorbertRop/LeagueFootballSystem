package DAO;

import Entities.Stadium;

import java.util.ArrayList;

public interface StadiumDAO {
	void addStadium(Stadium stadium);
	void removeStadium(Stadium stadium);
	void updateStadium(Stadium updatedStadium, Stadium insertedStadium);
	ArrayList<Stadium> getAllStadiums();
	ArrayList<Stadium> getStadiums(Stadium stadium);
}
