package DAO;

import Entities.Squad;

import java.util.ArrayList;

public interface SquadDAO {
	void addSquad(Squad squad);
	void removeSquad(Squad squad);
	ArrayList<Squad> getSquads(Squad squad);
}
