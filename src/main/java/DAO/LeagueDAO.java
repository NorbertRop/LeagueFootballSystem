package DAO;

import Entities.League;

import java.util.ArrayList;

public interface LeagueDAO {
	void addLeague(League league);
	void removeLeague(League league);
	void updateLeague(League updatedLeague, League insertedLeague);
	ArrayList<League> getAllLeagues();
	ArrayList<League> getLeagues(League league);
}
