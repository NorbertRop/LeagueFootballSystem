package DAO;

import DBUtils.DBConnection;
import Entities.Footballer;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class FootballerDAOImp implements FootballerDAO {

	private Connection connection;

	public FootballerDAOImp() {
		this.connection = DBConnection.getConnection();
	}

	@Override
	public void addFootballer(Footballer footballer) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
						"footballers(Name, Surname, Club, Goals, Assists, Birthday, Salary, Position, Height, Contract) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				preparedStatement.setString(1, footballer.getName());
				preparedStatement.setString(2, footballer.getSurname());
				preparedStatement.setString(3, footballer.getClub());
				if (footballer.getGoals() == -1) {
					preparedStatement.setInt(4, 0);
				} else {
					preparedStatement.setInt(4, footballer.getGoals());
				}
				if(footballer.getAssists() == -1){
					preparedStatement.setInt(5, 0);
				}else {
					preparedStatement.setInt(5, footballer.getAssists());
				}
				if(footballer.getSalary() == -1){
					preparedStatement.setInt(7, 0);
				}else {
					preparedStatement.setInt(7, footballer.getSalary());
				}
				if(footballer.getContract() == null){
					preparedStatement.setDate(10, null);
				} else {
					preparedStatement.setDate(10, footballer.getContract());
				}
				preparedStatement.setDate(6, footballer.getBirthday());
				preparedStatement.setString(8, footballer.getPosition());
				preparedStatement.setInt(9, footballer.getHeight());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				if (ex.getSQLState().equals("45000")) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Blad", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeFootballer(Footballer footballer) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM footballers " +
						"WHERE Name = ? AND Surname = ? AND Birthday = ? AND Position= ? AND Height = ?");
				preparedStatement.setString(1, footballer.getName());
				preparedStatement.setString(2, footballer.getSurname());
				preparedStatement.setDate(3, footballer.getBirthday());
				preparedStatement.setString(4, footballer.getPosition());
				preparedStatement.setInt(5, footballer.getHeight());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void updateFootballer(Footballer footballerUpdated, Footballer footballerInserted) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("UPDATE footballers " +
						"SET Name = ? , Surname = ? , Club = ? , Goals = ? , Assists = ? , Birthday = ? ,  Salary = ? " +
						", Position= ? , Height = ? , Contract = ? WHERE Name = ? AND Surname = ? AND Birthday = ? " +
						"AND Position = ? AND Height = ?");
				preparedStatement.setString(1, footballerInserted.getName());
				preparedStatement.setString(2, footballerInserted.getSurname());
				preparedStatement.setString(3, footballerInserted.getClub());
				if (footballerInserted.getGoals() == -1) {
					preparedStatement.setInt(4, 0);
				} else {
					preparedStatement.setInt(4, footballerInserted.getGoals());
				}
				if(footballerInserted.getAssists() == -1){
					preparedStatement.setInt(5, 0);
				}else {
					preparedStatement.setInt(5, footballerInserted.getAssists());
				}
				if(footballerInserted.getSalary() == -1){
					preparedStatement.setInt(7, 0);
				}else {
					preparedStatement.setInt(7, footballerInserted.getSalary());
				}
				if(footballerInserted.getContract() == null){
					preparedStatement.setDate(10, null);
				} else {
					preparedStatement.setDate(10, footballerInserted.getContract());
				}
				preparedStatement.setDate(6, footballerInserted.getBirthday());
				preparedStatement.setString(8, footballerInserted.getPosition());
				preparedStatement.setInt(9, footballerInserted.getHeight());

				preparedStatement.setString(11, footballerUpdated.getName());
				preparedStatement.setString(12, footballerUpdated.getSurname());
				preparedStatement.setDate(13, footballerUpdated.getBirthday());
				preparedStatement.setString(14, footballerUpdated.getPosition());
				preparedStatement.setInt(15, footballerUpdated.getHeight());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				if (ex.getSQLState().equals("45000")) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Blad", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<Footballer> getAllFootballers() {
		ArrayList<Footballer> footballersList = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM footballers");
			fillList(footballersList, resultSet);
		} catch (SQLException ex) {
		}
		return footballersList;
	}

	@Override
	public ArrayList<Footballer> getFootballers(Footballer f) {
		ArrayList<Footballer> footballersList = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM footballers WHERE Name LIKE ? AND Surname LIKE ? AND (Club LIKE ? OR Club IS NULL)" +
					"AND (Goals LIKE ? OR Goals IS NULL) AND (Assists LIKE ? OR Assists IS NULL) AND Birthday LIKE ? " +
					"AND (Salary LIKE ? OR Salary IS NULL) AND Position LIKE ? AND Height LIKE ? AND (Contract LIKE ? OR Contract IS NULL)");
			if (!(f.getName() == null)) {
				preparedStatement.setString(1, "%" + f.getName() + "%");
			} else {
				preparedStatement.setString(1, "%");
			}
			if (!(f.getSurname() == null)) {
				preparedStatement.setString(2, "%" + f.getSurname() + "%");
			} else {
				preparedStatement.setString(2, "%");
			}
			if(!(f.getClub() == null)){
				preparedStatement.setString(3, "%" + f.getClub() + "%");
			} else {
				preparedStatement.setString(3, "%");
			}
			if (!(f.getGoals() == -1)) {
				preparedStatement.setString(4, "%" + f.getGoals() + "%");
			} else {
				preparedStatement.setString(4, "%");
			}
			if (!(f.getAssists() == -1)) {
				preparedStatement.setString(5, "%" + f.getAssists() + "%");
			} else {
				preparedStatement.setString(5, "%");
			}
			if (!(f.getBirthday() == null)) {
				preparedStatement.setString(6, "%" + f.getBirthday() + "%");
			} else {
				preparedStatement.setString(6, "%");
			}
			if (!(f.getSalary() == -1)) {
				preparedStatement.setString(7, "%" + f.getSalary() + "%");
			} else {
				preparedStatement.setString(7, "%");
			}
			if (!(f.getPosition() == null)) {
				preparedStatement.setString(8, "%" + f.getPosition() + "%");
			} else {
				preparedStatement.setString(8, "%");
			}
			if (!(f.getHeight() == -1)) {
				preparedStatement.setString(9, "%" + f.getHeight() + "%");
			} else {
				preparedStatement.setString(9, "%");
			}
			if (!(f.getContract() == null)) {
				preparedStatement.setString(10, "%" + f.getContract() + "%");
			} else {
				preparedStatement.setString(10, "%");
			}

			ResultSet resultSet = preparedStatement.executeQuery();
			fillList(footballersList, resultSet);
		} catch (SQLException ex) {
		}
		return footballersList;
	}

	@Override
	public ArrayList<Footballer> getGoalKing() {
		ArrayList<Footballer> footballers = new ArrayList<>();
		try {
			//TU ZMIEN
			PreparedStatement preparedStatement = connection.prepareStatement("CALL goalsKing()");

			ResultSet resultSet = preparedStatement.executeQuery();
			fillList(footballers, resultSet);
			return  footballers;
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public ArrayList<Footballer> getAssistsKing() {
		ArrayList<Footballer> footballers = new ArrayList<>();
		try {
			//TU ZMIEN
			PreparedStatement preparedStatement = connection.prepareStatement("CALL assistsKing()");

			ResultSet resultSet = preparedStatement.executeQuery();
			fillList(footballers, resultSet);
			return  footballers;
		} catch (SQLException e) {
			return null;
		}
	}

	private void fillList(ArrayList<Footballer> footballersList, ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			Footballer footballer = new Footballer();
			footballer.setFootballerID(resultSet.getInt("FootballerID"));
			footballer.setName(resultSet.getString("Name"));
			footballer.setSurname(resultSet.getString("Surname"));
			footballer.setClub(resultSet.getString("Club"));
			footballer.setGoals(resultSet.getInt("Goals"));
			footballer.setAssists(resultSet.getInt("Assists"));
			footballer.setBirthday(resultSet.getDate("Birthday"));
			footballer.setSalary(resultSet.getInt("Salary"));
			footballer.setPosition(resultSet.getString("Position"));
			footballer.setHeight(resultSet.getInt("Height"));
			footballer.setContract(resultSet.getDate("Contract"));
			footballersList.add(footballer);
		}
	}
}
