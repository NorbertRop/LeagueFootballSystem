package DAO;

import DBUtils.DBConnection;
import Entities.Club;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class ClubDAOImp implements ClubDAO {
	private Connection connection;

	public ClubDAOImp(){
		connection = DBConnection.getConnection();
	}

	@Override
	public void addClub(Club club) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
						"clubs(FullName, Stadium, GoalsBalance, Wins, Losses, Draws, Points, League) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
				preparedStatement.setString(1, club.getFullName());
				preparedStatement.setString(2, club.getStadium());
				preparedStatement.setInt(3, club.getGoalsBalance());
				preparedStatement.setInt(4, club.getWins());
				preparedStatement.setInt(5, club.getLosses());
				preparedStatement.setInt(6, club.getDraws());
				preparedStatement.setInt(7, club.getPoints());
				preparedStatement.setString(8, club.getLeague());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				if (ex.getSQLState().equals("45000")) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Blad", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeClub(Club club) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM clubs " +
						"WHERE FullName = ? AND Stadium = ? AND GoalsBalance = ? AND Wins = ? " +
						"AND Losses = ? AND Draws = ? AND Points = ? AND League = ?");
				preparedStatement.setString(1, club.getFullName());
				preparedStatement.setString(2, club.getStadium());
				preparedStatement.setInt(3, club.getGoalsBalance());
				preparedStatement.setInt(4, club.getWins());
				preparedStatement.setInt(5, club.getLosses());
				preparedStatement.setInt(6, club.getDraws());
				preparedStatement.setInt(7, club.getPoints());
				preparedStatement.setString(8, club.getLeague());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void updateClub(Club updatedClub, Club insertedClub) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("UPDATE clubs " +
						"SET FullName = ?, Stadium = ?, GoalsBalance = ?, Wins = ?, Losses = ?, Draws = ? " +
						", Points = ?, League = ? WHERE FullName = ? AND Stadium = ? AND GoalsBalance = ? AND Wins = ? " +
						"AND Losses = ? AND Draws = ? AND Points = ? AND League = ?");
				preparedStatement.setString(1, insertedClub.getFullName());
				preparedStatement.setString(2, insertedClub.getStadium());
				preparedStatement.setInt(3, insertedClub.getGoalsBalance());
				preparedStatement.setInt(4, insertedClub.getWins());
				preparedStatement.setInt(5, insertedClub.getLosses());
				preparedStatement.setInt(6, insertedClub.getDraws());
				preparedStatement.setInt(7, insertedClub.getPoints());
				preparedStatement.setString(8, insertedClub.getLeague());

				preparedStatement.setString(9, updatedClub.getFullName());
				preparedStatement.setString(10, updatedClub.getStadium());
				preparedStatement.setInt(11, updatedClub.getGoalsBalance());
				preparedStatement.setInt(12, updatedClub.getWins());
				preparedStatement.setInt(13, updatedClub.getLosses());
				preparedStatement.setInt(14, updatedClub.getDraws());
				preparedStatement.setInt(15, updatedClub.getPoints());
				preparedStatement.setString(16, updatedClub.getLeague());

				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				if (ex.getSQLState().equals("45000")) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Blad", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<Club> getAllClubs() {
		ArrayList<Club> clubList = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM clubs");
			fillList(clubList, resultSet);
		} catch (SQLException ex) {
		}

		return clubList;
	}

	@Override
	public ArrayList<Club> getClubs(Club c) {
		ArrayList<Club> clubList = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM clubs WHERE FullName LIKE ? " +
					"AND Stadium LIKE ? AND League LIKE ?");
			if(!(c.getFullName() == null)) {
				preparedStatement.setString(1, "%" + c.getFullName() + "%");
			}else {
				preparedStatement.setString(1, "%");
			}
			if(!(c.getStadium() == null)) {
				preparedStatement.setString(2, "%" + c.getStadium() + "%");
			}else {
				preparedStatement.setString(2, "%");
			}
			if(!(c.getLeague() == null)) {
				preparedStatement.setString(3, "%" + c.getLeague() + "%");
			}else {
				preparedStatement.setString(3, "%");
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			fillList(clubList, resultSet);
		} catch (SQLException ex) {
		}

		return clubList;
	}

	private void fillList(ArrayList<Club> clubList, ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			Club club = new Club();
			club.setFullName(resultSet.getString("FullName"));
			club.setStadium(resultSet.getString("Stadium"));
			club.setGoalsBalance(resultSet.getInt("GoalsBalance"));
			club.setWins(resultSet.getInt("Wins"));
			club.setLosses(resultSet.getInt("Losses"));
			club.setDraws(resultSet.getInt("Draws"));
			club.setPoints(resultSet.getInt("Points"));
			club.setLeague(resultSet.getString("League"));
			clubList.add(club);
		}
	}
}
