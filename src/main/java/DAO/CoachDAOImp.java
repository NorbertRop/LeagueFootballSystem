package DAO;

import DBUtils.DBConnection;
import Entities.Coach;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class CoachDAOImp implements CoachDAO {
	private Connection connection;

	public CoachDAOImp() {
		connection = DBConnection.getConnection();
	}

	@Override
	public void addCoach(Coach coach) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
						"coaches(Name, Surname, Club, Specialization) VALUES (?, ?, ?, ?)");
				preparedStatement.setString(1, coach.getName());
				preparedStatement.setString(2, coach.getSurname());
				if (coach.getClub().equals("")) {
					preparedStatement.setString(3, null);
				} else {
					preparedStatement.setString(3, coach.getClub());
				}
				preparedStatement.setString(4, coach.getSpecialization());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Błędne dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeCoach(Coach coach) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM coaches " +
						"WHERE Name = ? AND Surname = ? AND Club LIKE ? AND Specialization = ?");
				preparedStatement.setString(1, coach.getName());
				preparedStatement.setString(2, coach.getSurname());
				if (coach.getClub().equals("")) {
					preparedStatement = connection.prepareStatement("DELETE FROM coaches " +
							"WHERE Name = ? AND Surname = ? AND (Club LIKE '%' OR Club IS NULL) AND Specialization = ?");
					preparedStatement.setString(1, coach.getName());
					preparedStatement.setString(2, coach.getSurname());
					preparedStatement.setString(3, coach.getSpecialization());
				} else {
					preparedStatement.setString(3, coach.getClub());
					preparedStatement.setString(4, coach.getSpecialization());
				}
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void updateCoach(Coach updatedCoach, Coach insertedCoach) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("UPDATE coaches " +
						"SET Name = ?, Surname = ?, Club = ?, Specialization = ? WHERE Name = ? AND Surname = ? AND Specialization = ?");
				preparedStatement.setString(1, insertedCoach.getName());
				preparedStatement.setString(2, insertedCoach.getSurname());
				if (insertedCoach.getClub().equals("")) {
					preparedStatement.setString(3, null);
				} else {
					preparedStatement.setString(3, insertedCoach.getClub());
				}
				preparedStatement.setString(4, insertedCoach.getSpecialization());
				preparedStatement.executeUpdate();

				preparedStatement.setString(5, updatedCoach.getName());
				preparedStatement.setString(6, updatedCoach.getSurname());
				preparedStatement.setString(7, updatedCoach.getSpecialization());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<Coach> getAllCoaches() {
		ArrayList<Coach> coachList = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM coaches");
			fillList(coachList, resultSet);
		} catch (SQLException ex) {

		}

		return coachList;
	}

	@Override
	public ArrayList<Coach> getCoaches(Coach c) {
		ArrayList<Coach> coachList = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM coaches WHERE Name LIKE ? AND Surname LIKE  ? AND (Club LIKE  ? OR Club IS NULL)" +
					" AND Specialization LIKE  ?");
			if (!(c.getName() == null)) {
				preparedStatement.setString(1, "%" + c.getName() + "%");
			} else {
				preparedStatement.setString(1, "%");
			}
			if (!(c.getSurname() == null)) {
				preparedStatement.setString(2, "%" + c.getSurname() + "%");
			} else {
				preparedStatement.setString(2, "%");
			}
			if (!(c.getClub() == null)) {
				preparedStatement.setString(3, "%" + c.getClub() + "%");
			} else {
				preparedStatement.setString(3, "%");
			}
			if (!(c.getSpecialization() == null)) {
				preparedStatement.setString(4, "%" + c.getSpecialization() + "%");
			} else {
				preparedStatement.setString(4, "%");
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			fillList(coachList, resultSet);
		} catch (SQLException ex) {
		}

		return coachList;
	}

	private void fillList(ArrayList<Coach> coachList, ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			Coach coach = new Coach();
			coach.setName(resultSet.getString("Name"));
			coach.setSurname(resultSet.getString("Surname"));
			coach.setClub(resultSet.getString("Club"));
			coach.setSpecialization(resultSet.getString("Specialization"));
			coachList.add(coach);
		}
	}
}
