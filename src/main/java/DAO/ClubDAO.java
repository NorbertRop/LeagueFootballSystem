package DAO;

import Entities.Club;

import java.util.ArrayList;

public interface ClubDAO {
	void addClub(Club club);
	void removeClub(Club club);
	void updateClub(Club updatedClub, Club insertedClub);
	ArrayList<Club> getAllClubs();
	ArrayList<Club> getClubs(Club club);
}
