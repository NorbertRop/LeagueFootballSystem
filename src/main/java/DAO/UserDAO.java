package DAO;

import DBUtils.DBConnection;
import DBUtils.Passwords;
import Entities.User;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO {
	private Connection connection;

	public UserDAO() {
		this.connection = DBConnection.getConnection();
	}

	public void addUser(User user) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Us(Nick, Password, Salt, Type, FavTeam) VALUES (?, ?, ?, ?, ?)");
				preparedStatement.setString(1, user.getNick());
				preparedStatement.setBytes(2, user.getPass());
				preparedStatement.setBytes(3, user.getSalt());
				preparedStatement.setString(4, user.getType());
				preparedStatement.setString(5, user.getFavTeam());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	public User loginUser(String nick, char[] pass) {
		User user = new User();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT Password, Salt, Type, FavTeam FROM Us WHERE Nick = ?");
			preparedStatement.setString(1, nick);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			user.setPass(resultSet.getBytes("Password"));
			user.setSalt(resultSet.getBytes("Salt"));
			user.setType(resultSet.getString("Type"));
			user.setFavTeam(resultSet.getString("FavTeam"));
		} catch (SQLException ex) {
			return null;
		}
		if(Passwords.isExpectedPassword(pass, user.getSalt(), user.getPass())){
			return user;
		}else {
			return null;
		}
	}
}
