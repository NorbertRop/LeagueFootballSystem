package DAO;

import DBUtils.DBConnection;
import Entities.Footballer;
import Entities.League;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class LeagueDAOImp implements LeagueDAO {
	private Connection connection;

	public LeagueDAOImp(){
		connection = DBConnection.getConnection();
	}

	@Override
	public void addLeague(League league) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO leagues(LeagueName) VALUES (?)");
				preparedStatement.setString(1, league.getName());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeLeague(League league) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM leagues WHERE LeagueName = ?");
				preparedStatement.setString(1, league.getName());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void updateLeague(League updatedLeague, League insertedLeague) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("UPDATE leagues SET LeagueName = ? WHERE LeagueName = ?");
				preparedStatement.setString(1, insertedLeague.getName());
				preparedStatement.setString(2, updatedLeague.getName());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<League> getAllLeagues() {
		ArrayList<League> leagueList = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM leagues");
			while (resultSet.next()) {
				League league = new League();
				league.setName(resultSet.getString("LeagueName"));
				leagueList.add(league);
			}
		} catch (SQLException ex) {
		}

		return leagueList;
	}

	@Override
	public ArrayList<League> getLeagues(League league) {
		ArrayList<League> leagueList = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM leagues WHERE LeagueName LIKE ?");

			if(!(league.getName() == null)) {
				preparedStatement.setString(1, "%" + league.getName() + "%");
			}else {
				preparedStatement.setString(1, "%");
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				League l = new League();
				l.setName(resultSet.getString("LeagueName"));
				leagueList.add(l);
			}
		} catch (SQLException ex) {
		}

		return leagueList;
	}
}
