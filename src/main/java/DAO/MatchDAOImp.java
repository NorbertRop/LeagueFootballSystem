package DAO;

import DBUtils.DBConnection;
import Entities.Match;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public class MatchDAOImp implements MatchDAO {
	private Connection connection;

	public MatchDAOImp() {
		connection = DBConnection.getConnection();
	}

	@Override
	public void addMatch(Match match) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
						"matches(Team1, Team2, HomeTeam, MatchTime, League, ShootsTeam1, ShootsTeam2, PossessionTeam1, PossessionTeam2," +
						" POTM, Round) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				preparedStatement.setString(1, match.getTeam1());
				preparedStatement.setString(2, match.getTeam2());
				preparedStatement.setString(3, match.getHomeTeam());
				preparedStatement.setDate(4, match.getMatchTime());
				preparedStatement.setString(5, match.getLeague());
				if (match.getShootsTeam1() == -1) {
					preparedStatement.setString(6, null);
				} else {
					preparedStatement.setInt(6, match.getShootsTeam1());
				}
				if (match.getShootsTeam2() == -1) {
					preparedStatement.setString(7, null);
				} else {
					preparedStatement.setInt(7, match.getShootsTeam2());
				}
				if (match.getPossessionTeam1() == -1) {
					preparedStatement.setString(8, null);
				} else {
					preparedStatement.setInt(8, match.getPossessionTeam1());
				}
				if (match.getPossessionTeam2() == -1) {
					preparedStatement.setString(9, null);
				} else {
					preparedStatement.setInt(9, match.getPossessionTeam2());
				}
				if (match.getPOTM() == -1) {
					preparedStatement.setString(10, null);
				} else {
					preparedStatement.setInt(10, match.getPOTM());
				}
				preparedStatement.setInt(11, match.getRound());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				if (ex.getSQLState().equals("45000")) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Blad", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeMatch(Match match) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM matches " +
						"WHERE Team1 = ? AND Team2 = ? AND HomeTeam = ? AND MatchTime = ? AND League = ? " +
						"AND Round = ?");
				preparedStatement.setString(1, match.getTeam1());
				preparedStatement.setString(2, match.getTeam2());
				preparedStatement.setString(3, match.getHomeTeam());
				preparedStatement.setDate(4, match.getMatchTime());
				preparedStatement.setString(5, match.getLeague());
				preparedStatement.setInt(6, match.getRound());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void updateMatch(Match updatedMatch, Match insertedMatch) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("UPDATE matches " +
						"SET Team1 = ? AND Team2 = ? AND HomeTeam = ? AND MatchTime = ? AND League = ? AND ShootsTeam1 = ? " +
						"AND ShootsTeam2 = ? AND PossessionTeam1 = ? AND PossessionTeam2 = ? AND POTM = ? AND Round = ? WHERE " +
						"Team1 = ? AND Team2 = ? AND HomeTeam = ? AND MatchTime = ? AND League = ? " +
						"AND Round = ?");
				preparedStatement.setString(1, insertedMatch.getTeam1());
				preparedStatement.setString(2, insertedMatch.getTeam2());
				preparedStatement.setString(3, insertedMatch.getHomeTeam());
				preparedStatement.setDate(4, insertedMatch.getMatchTime());
				preparedStatement.setString(5, insertedMatch.getLeague());
				if (insertedMatch.getShootsTeam1() == -1) {
					preparedStatement.setString(6, null);
				} else {
					preparedStatement.setInt(6, insertedMatch.getShootsTeam1());
				}
				if (insertedMatch.getShootsTeam2() == -1) {
					preparedStatement.setString(7, null);
				} else {
					preparedStatement.setInt(7, insertedMatch.getShootsTeam2());
				}
				if (insertedMatch.getPossessionTeam1() == -1) {
					preparedStatement.setString(8, null);
				} else {
					preparedStatement.setInt(8, insertedMatch.getPossessionTeam1());
				}
				if (insertedMatch.getPossessionTeam2() == -1) {
					preparedStatement.setString(9, null);
				} else {
					preparedStatement.setInt(9, insertedMatch.getPossessionTeam2());
				}
				if (insertedMatch.getPOTM() == -1) {
					preparedStatement.setString(10, null);
				} else {
					preparedStatement.setInt(10, insertedMatch.getPOTM());
				}
				preparedStatement.setInt(11, insertedMatch.getRound());


				preparedStatement.setString(12, updatedMatch.getTeam1());
				preparedStatement.setString(13, updatedMatch.getTeam2());
				preparedStatement.setString(14, updatedMatch.getHomeTeam());
				preparedStatement.setDate(15, updatedMatch.getMatchTime());
				preparedStatement.setString(16, updatedMatch.getLeague());
				preparedStatement.setInt(17, updatedMatch.getRound());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				if (ex.getSQLState().equals("45000")) {
					JOptionPane.showMessageDialog(null, ex.getMessage(), "Blad", JOptionPane.ERROR_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
				}
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<Match> getAllMatches() {
		ArrayList<Match> matchList = new ArrayList<>();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM matches");
			fillList(matchList, resultSet);
		} catch (SQLException ex) {
		}

		return matchList;
	}

	@Override
	public ArrayList<Match> getMatches(Match m) {
		ArrayList<Match> matchList = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM matches WHERE Team1 LIKE ? AND Team2 LIKE ? " +
					"AND HomeTeam LIKE ? AND MatchTime LIKE ? AND League LIKE ? AND (ShootsTeam1 LIKE ? OR ShootsTeam1 IS NULL) AND (ShootsTeam2 LIKE ? OR ShootsTeam2 IS NULL) " +
					"AND (PossessionTeam1 LIKE ? OR PossessionTeam1 IS NULL) AND (PossessionTeam2 LIKE ? OR PossessionTeam2 IS NULL) AND (POTM LIKE ? OR POTM IS NULL) AND Round LIKE ?");
			if (!(m.getTeam1() == null)) {
				preparedStatement.setString(1, "%" + m.getTeam1() + "%");
			} else {
				preparedStatement.setString(1, "%");
			}
			if (!(m.getTeam2() == null)) {
				preparedStatement.setString(2, "%" + m.getTeam2() + "%");
			} else {
				preparedStatement.setString(2, "%");
			}
			if (!(m.getHomeTeam() == null)) {
				preparedStatement.setString(3, "%" + m.getHomeTeam() + "%");
			} else {
				preparedStatement.setString(3, "%");
			}
			if (!(m.getMatchTime() == null)) {
				preparedStatement.setString(4, "%" + m.getMatchTime() + "%");
			} else {
				preparedStatement.setString(4, "%");
			}
			if (!(m.getLeague() == null)) {
				preparedStatement.setString(5, "%" + m.getLeague() + "%");
			} else {
				preparedStatement.setString(5, "%");
			}
			if (!(m.getShootsTeam1() == -1)) {
				preparedStatement.setString(6, "%" + m.getShootsTeam1() + "%");
			} else {
				preparedStatement.setString(6, "%");
			}
			if (!(m.getShootsTeam2() == -1)) {
				preparedStatement.setString(7, "%" + m.getShootsTeam2() + "%");
			} else {
				preparedStatement.setString(7, "%");
			}
			if (!(m.getPossessionTeam1() == -1)) {
				preparedStatement.setString(8, "%" + m.getPossessionTeam1() + "%");
			} else {
				preparedStatement.setString(8, "%");
			}
			if (!(m.getPossessionTeam2() == -1)) {
				preparedStatement.setString(9, "%" + m.getPossessionTeam2() + "%");
			} else {
				preparedStatement.setString(9, "%");
			}
			if (!(m.getPOTM() == -1)) {
				preparedStatement.setString(10, "%" + m.getPOTM() + "%");
			} else {
				preparedStatement.setString(10, "%");
			}
			if (!(m.getRound() == -1)) {
				preparedStatement.setString(11, "%" + m.getRound() + "%");
			} else {
				preparedStatement.setString(11, "%");
			}

			ResultSet resultSet = preparedStatement.executeQuery();
			fillList(matchList, resultSet);
		} catch (SQLException ex) {
		}
		return matchList;
	}

	private void fillList(ArrayList<Match> matchList, ResultSet resultSet) throws SQLException {
		while (resultSet.next()) {
			Match match = new Match();
			match.setMatchID(resultSet.getInt("MatchID"));
			match.setTeam1(resultSet.getString("Team1"));
			match.setTeam2(resultSet.getString("Team2"));
			match.setHomeTeam(resultSet.getString("HomeTeam"));
			match.setMatchTime(resultSet.getDate("MatchTime"));
			match.setLeague(resultSet.getString("League"));
			match.setShootsTeam1(resultSet.getInt("ShootsTeam1"));
			match.setShootsTeam2(resultSet.getInt("ShootsTeam2"));
			match.setPossessionTeam1(resultSet.getInt("PossessionTeam1"));
			match.setPossessionTeam2(resultSet.getInt("PossessionTeam2"));
			match.setPOTM(resultSet.getInt("POTM"));
			match.setRound(resultSet.getInt("Round"));
			matchList.add(match);
		}
	}

	public Date getNextMatch(String team){
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("CALL getNextMatch(?)");
			preparedStatement.setString(1, team);

			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				if(resultSet.getDate("Time") != null)
					return resultSet.getDate("Time");
				else
					return null;
			} else{
				return null;
			}
		} catch (SQLException e) {
			return null;
		}
	}

	@Override
	public ArrayList<Match> getRound(int round) {
		PreparedStatement preparedStatement = null;
		ArrayList<Match> matches = new ArrayList<>();
		try {
			preparedStatement = connection.prepareStatement("CALL getRound(?)");
			preparedStatement.setInt(1, round);

			ResultSet resultSet = preparedStatement.executeQuery();
			fillList(matches, resultSet);
			return matches;
		} catch (SQLException e) {
			return null;
		}
	}
}
