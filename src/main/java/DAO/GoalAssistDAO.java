package DAO;

import Entities.GoalAssist;

import java.util.ArrayList;

public interface GoalAssistDAO {
	void addGoalAssist(GoalAssist goalAssist);
	void removeGoalAssist(GoalAssist goalAssist);
	ArrayList<GoalAssist> getGoals(GoalAssist goalAssist);
}
