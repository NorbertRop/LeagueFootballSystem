package DAO;

import DBUtils.DBConnection;
import Entities.Squad;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SquadDAOImp implements SquadDAO {
	private Connection connection;

	public SquadDAOImp(){
		this.connection = DBConnection.getConnection();
	}
	@Override
	public void addSquad(Squad squad) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
						"squads(Footballer, MatchID, StartingEleven) VALUES (?, ?, ?)");
				preparedStatement.setInt(1, squad.getFootballer());
				preparedStatement.setInt(2, squad.getMatch());
				preparedStatement.setBoolean(3, squad.isStartingEleven());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeSquad(Squad squad) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM squads " +
						"WHERE Footballer = ? AND MatchID = ? AND StartingEleven = ?");
				preparedStatement.setInt(1, squad.getFootballer());
				preparedStatement.setInt(2, squad.getMatch());
				preparedStatement.setBoolean(3, squad.isStartingEleven());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<Squad> getSquads(Squad squad) {
		ArrayList<Squad> squads = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM squads WHERE Footballer LIKE ? AND" +
					" MatchID LIKE  ? AND StartingEleven LIKE ?");
			if (!(squad.getFootballer() == -1)) {
				preparedStatement.setString(1, "%" + squad.getFootballer() + "%");
			} else {
				preparedStatement.setString(1, "%");
			}
			if (!(squad.getMatch() == -1)) {
				preparedStatement.setString(2, "%" + squad.getMatch() + "%");
			} else {
				preparedStatement.setString(2, "%");
			}
			if (!(squad.isStartingEleven() == null)) {
				preparedStatement.setBoolean(3, squad.isStartingEleven());
			} else {
				preparedStatement.setString(3, "%");
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Squad s = new Squad();
				s.setFootballer(resultSet.getInt("Footballer"));
				s.setMatch(resultSet.getInt("MatchID"));
				s.setStartingEleven(resultSet.getBoolean("StartingEleven"));
				squads.add(s);
			}
		} catch (SQLException ex) {
		}
		return squads;
	}
}
