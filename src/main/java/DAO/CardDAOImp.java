package DAO;

import DBUtils.DBConnection;
import Entities.Card;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CardDAOImp implements CardDAO {
	private Connection connection;

	public CardDAOImp() {
		this.connection = DBConnection.getConnection();
	}

	@Override
	public void addCard(Card card) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
						"cards(Footballer, Type, MatchID) VALUES (?, ?, ?)");
				preparedStatement.setInt(1, card.getFootballer());
				preparedStatement.setString(2, card.getType());
				preparedStatement.setInt(3, card.getMatch());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeCard(Card card) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM cards " +
						"WHERE Footballer = ? AND Type = ? AND MatchID = ?");
				preparedStatement.setInt(1, card.getFootballer());
				preparedStatement.setString(2, card.getType());
				preparedStatement.setInt(3, card.getMatch());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<Card> getCards(Card c) {
		ArrayList<Card> cardList = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM cards WHERE Footballer LIKE ? AND Type LIKE  ? AND" +
					" MatchID LIKE  ?");
			if (!(c.getFootballer() == -1)) {
				preparedStatement.setString(1, "%" + c.getFootballer() + "%");
			} else {
				preparedStatement.setString(1, "%");
			}
			if (!(c.getType() == null)) {
				preparedStatement.setString(2, c.getType());
			} else {
				preparedStatement.setString(2, "%");
			}
			if (!(c.getMatch() == -1)) {
				preparedStatement.setString(3, "%" + c.getMatch() + "%");
			} else {
				preparedStatement.setString(3, "%");
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Card card = new Card();
				card.setFootballer(resultSet.getInt("Footballer"));
				card.setType(resultSet.getString("Type"));
				card.setMatch(resultSet.getInt("MatchID"));
				cardList.add(card);
			}
		} catch (SQLException ex) {
		}
		return cardList;
	}
}
