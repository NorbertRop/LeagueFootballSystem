package DAO;

import DBUtils.DBConnection;
import Entities.GoalAssist;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GoalAssistDAOImp implements GoalAssistDAO {
	private Connection connection;

	public GoalAssistDAOImp() {
		this.connection = DBConnection.getConnection();
	}

	@Override
	public void addGoalAssist(GoalAssist goalAssist) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO " +
						"goalsassists(Footballer, Type, MatchID) VALUES (?, ?, ?)");
				preparedStatement.setInt(1, goalAssist.getFootballer());
				preparedStatement.setString(2, goalAssist.getType());
				preparedStatement.setInt(3, goalAssist.getMatch());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public void removeGoalAssist(GoalAssist goalAssist) {
		try {
			try {
				PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM goalsassists " +
						"WHERE Footballer = ? AND Type = ? AND MatchID = ?");
				preparedStatement.setInt(1, goalAssist.getFootballer());
				preparedStatement.setString(2, goalAssist.getType());
				preparedStatement.setInt(3, goalAssist.getMatch());
				preparedStatement.executeUpdate();
				connection.commit();
			} catch (SQLException ex) {
				connection.rollback();
				JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException shouldNotBeHandled) {
			//...
		}
	}

	@Override
	public ArrayList<GoalAssist> getGoals(GoalAssist g) {
		ArrayList<GoalAssist> goalAssists = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM goalsassists WHERE Footballer LIKE ? AND Type LIKE  ? AND" +
					" MatchID LIKE  ?");
			if (!(g.getFootballer() == -1)) {
				preparedStatement.setString(1, "%" + g.getFootballer() + "%");
			} else {
				preparedStatement.setString(1, "%");
			}
			if (!(g.getType() == null)) {
				preparedStatement.setString(2, g.getType());
			} else {
				preparedStatement.setString(2, "%");
			}
			if (!(g.getMatch() == -1)) {
				preparedStatement.setString(3, "%" + g.getMatch() + "%");
			} else {
				preparedStatement.setString(3, "%");
			}
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				GoalAssist ga = new GoalAssist();
				ga.setFootballer(resultSet.getInt("Footballer"));
				ga.setType(resultSet.getString("Type"));
				ga.setMatch(resultSet.getInt("MatchID"));
				goalAssists.add(ga);
			}
		} catch (SQLException ex) {
		}
		return goalAssists;
	}
}
