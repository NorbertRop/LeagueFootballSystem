package DAO;

import Entities.Coach;

import java.util.ArrayList;

public interface CoachDAO {
	void addCoach(Coach coach);
	void removeCoach(Coach coach);
	void updateCoach(Coach updatedCoach, Coach insertedCoach);
	ArrayList<Coach> getAllCoaches();
	ArrayList<Coach> getCoaches(Coach coach);
}
