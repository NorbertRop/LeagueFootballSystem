package View;

import DAO.LeagueDAO;
import DAO.LeagueDAOImp;
import Entities.League;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class GuestLeagueDialog extends JDialog {
	private LeagueDAO leagueDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField nameTextField;

	public GuestLeagueDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(400, 250);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Ligi");

		leagueDAO = new LeagueDAOImp();

		prepareTextFieldPanel();
		prepareAcceptingButtonPanel();

		add(textFieldsPanel);
		add(acceptingButtonPanel);
	}

	private void prepareTextFieldPanel() {
		nameTextField = new JTextField(15);

		JLabel nameLabel = new JLabel("Nazwa");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(2, 1, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(300, 100));
		textFieldsPanel.add(nameLabel);
		textFieldsPanel.add(nameTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(200, 150));

		JButton showButton = new JButton("Pokaz ligi");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(showButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("Nazwa\n");
			League l = new League();
			ArrayList<League> leagueList;
			if (nameTextField.getText().equals("")) {
				leagueList = leagueDAO.getAllLeagues();
			} else {
				if (!nameTextField.getText().equals("")) {
					l.setName(nameTextField.getText());
				}
				leagueList = leagueDAO.getLeagues(l);
			}
			for (League league : leagueList) {
				String stringBuilder = league.getName() + "\n";
				((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}
}
