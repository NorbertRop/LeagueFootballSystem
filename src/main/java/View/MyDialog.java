package View;

import javax.swing.*;
import java.awt.*;

public class MyDialog extends JDialog {
	protected JPanel radioBoxPanel;
	protected ButtonGroup buttonGroup;

	public MyDialog(JFrame owner){
		super(owner);
	}

	protected void prepareRadioBox() {
		radioBoxPanel = new JPanel(new GridLayout(1, 3, 20, 10));
		radioBoxPanel.setPreferredSize(new Dimension(600, 100));
		buttonGroup = new ButtonGroup();

		JRadioButton addButton = new JRadioButton("Dodaj", true);
		addButton.setActionCommand("Dodaj");
		addButton.setFont(addButton.getFont().deriveFont(30.0f));
		addButton.addActionListener(event -> switchButtons(false));
		buttonGroup.add(addButton);
		radioBoxPanel.add(addButton);

		JRadioButton deleteButton = new JRadioButton("Usun", false);
		deleteButton.setActionCommand("Usun");
		deleteButton.setFont(addButton.getFont().deriveFont(30.0f));
		deleteButton.addActionListener(event -> switchButtons(false));
		buttonGroup.add(deleteButton);
		radioBoxPanel.add(deleteButton);


		JRadioButton updateButton = new JRadioButton("Modyfikuj", false);
		updateButton.setActionCommand("Modyfikuj");
		updateButton.setFont(addButton.getFont().deriveFont(30.0f));
		updateButton.addActionListener(event -> switchButtons(true));
		buttonGroup.add(updateButton);
		radioBoxPanel.add(updateButton);
	}

	protected void switchButtons(boolean b){

	}
}
