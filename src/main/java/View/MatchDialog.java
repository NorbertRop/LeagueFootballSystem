package View;

import DAO.CardDAO;
import DAO.CardDAOImp;
import DAO.MatchDAO;
import DAO.MatchDAOImp;
import Entities.Match;
import org.omg.CORBA.BAD_INV_ORDER;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.util.ArrayList;

public class MatchDialog extends MyDialog {
	private MatchDAO matchDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextArea display;
	private JScrollPane scrollPane;
	private JTextField team1TextField;
	private JTextField team2TextField;
	private JTextField homeTeamTextField;
	private JTextField matchTimeTextField;
	private JTextField leagueTextField;
	private JTextField shootsTeam1TextField;
	private JTextField shootsTeam2TeamTextField;
	private JTextField possessionTeam1TextField;
	private JTextField possessionTeam2TextField;
	private JTextField POTMTextField;
	private JTextField roundTextField;

	private JTextField team1UpdatedTextField;
	private JTextField team2UpdatedTextField;
	private JTextField homeUpdatedTeamTextField;
	private JTextField matchUpdatedTimeTextField;
	private JTextField leagueUpdatedTextField;
	private JTextField shootsTeam1UpdatedTextField;
	private JTextField shootsTeam2UpdatedTeamTextField;
	private JTextField possessionTeam1UpdatedTextField;
	private JTextField possessionTeam2UpdatedTextField;
	private JTextField POTMTextUpdatedField;
	private JTextField roundTextUpdatedField;

	public MatchDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(1450, 800);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Zarządzanie meczem");

		matchDAO = new MatchDAOImp();

		prepareTextFieldPanel();
		prepareRadioBox();
		prepareAcceptingButtonPanel();

		display = new JTextArea();
		display.setEditable(false);
		display.setFont(display.getFont().deriveFont(24.0f));
		scrollPane = new JScrollPane(display);
		scrollPane.setPreferredSize(new Dimension(new Dimension(500, 700)));
		add(radioBoxPanel, BorderLayout.PAGE_START);
		add(scrollPane, BorderLayout.LINE_END);
		add(textFieldsPanel, BorderLayout.CENTER);
		add(acceptingButtonPanel, BorderLayout.PAGE_END);
	}

	private void prepareTextFieldPanel() {
		team1TextField = new JTextField();
		team2TextField = new JTextField();
		homeTeamTextField = new JTextField();
		matchTimeTextField = new JTextField();
		leagueTextField = new JTextField();
		shootsTeam1TextField = new JTextField();
		shootsTeam2TeamTextField = new JTextField();
		possessionTeam1TextField = new JTextField();
		possessionTeam2TextField = new JTextField();
		POTMTextField = new JTextField();
		roundTextField = new JTextField();

		team1UpdatedTextField = new JTextField();
		team1UpdatedTextField.setEditable(false);
		team2UpdatedTextField = new JTextField();
		team2UpdatedTextField.setEditable(false);
		homeUpdatedTeamTextField = new JTextField();
		homeUpdatedTeamTextField.setEditable(false);
		matchUpdatedTimeTextField = new JTextField();
		matchUpdatedTimeTextField.setEditable(false);
		leagueUpdatedTextField = new JTextField();
		leagueUpdatedTextField.setEditable(false);
		shootsTeam1UpdatedTextField = new JTextField();
		shootsTeam1UpdatedTextField.setEditable(false);
		shootsTeam2UpdatedTeamTextField = new JTextField();
		shootsTeam2UpdatedTeamTextField.setEditable(false);
		possessionTeam1UpdatedTextField = new JTextField();
		possessionTeam1UpdatedTextField.setEditable(false);
		possessionTeam2UpdatedTextField = new JTextField();
		possessionTeam2UpdatedTextField.setEditable(false);
		POTMTextUpdatedField = new JTextField();
		POTMTextUpdatedField.setEditable(false);
		roundTextUpdatedField = new JTextField();
		roundTextUpdatedField.setEditable(false);

		JLabel team1Label = new JLabel("Drużyna 1");
		JLabel team2Label = new JLabel("Drużyna 2");
		JLabel homeTeamLabel = new JLabel("Gospodarz");
		JLabel matchTimeLabel = new JLabel("Data meczu");
		JLabel leagueLabel = new JLabel("Liga");
		JLabel shootsTeam1Label = new JLabel("Strzały Drużyny 1");
		JLabel shootsTeam2Label = new JLabel("Strzały Drużyny 2");
		JLabel possessionTeam1Label = new JLabel("Posiadanie piłki 1");
		JLabel possessionTeam2Label = new JLabel("Posiadanie piłki 2");
		JLabel POTMLabel = new JLabel("POTM");
		JLabel roundLabel = new JLabel("Round");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(11, 3, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(500, 600));

		textFieldsPanel.add(team1Label);
		textFieldsPanel.add(team1TextField);
		textFieldsPanel.add(team1UpdatedTextField);

		textFieldsPanel.add(team2Label);
		textFieldsPanel.add(team2TextField);
		textFieldsPanel.add(team2UpdatedTextField);

		textFieldsPanel.add(homeTeamLabel);
		textFieldsPanel.add(homeTeamTextField);
		textFieldsPanel.add(homeUpdatedTeamTextField);

		textFieldsPanel.add(matchTimeLabel);
		textFieldsPanel.add(matchTimeTextField);
		textFieldsPanel.add(matchUpdatedTimeTextField);

		textFieldsPanel.add(leagueLabel);
		textFieldsPanel.add(leagueTextField);
		textFieldsPanel.add(leagueUpdatedTextField);

		textFieldsPanel.add(shootsTeam1Label);
		textFieldsPanel.add(shootsTeam1TextField);
		textFieldsPanel.add(shootsTeam1UpdatedTextField);

		textFieldsPanel.add(shootsTeam2Label);
		textFieldsPanel.add(shootsTeam2TeamTextField);
		textFieldsPanel.add(shootsTeam2UpdatedTeamTextField);

		textFieldsPanel.add(possessionTeam1Label);
		textFieldsPanel.add(possessionTeam1TextField);
		textFieldsPanel.add(possessionTeam1UpdatedTextField);

		textFieldsPanel.add(possessionTeam2Label);
		textFieldsPanel.add(possessionTeam2TextField);
		textFieldsPanel.add(possessionTeam2UpdatedTextField);

		textFieldsPanel.add(POTMLabel);
		textFieldsPanel.add(POTMTextField);
		textFieldsPanel.add(POTMTextUpdatedField);

		textFieldsPanel.add(roundLabel);
		textFieldsPanel.add(roundTextField);
		textFieldsPanel.add(roundTextUpdatedField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(250, 100));
		JButton commitButton = new JButton("Zatwierdz");
		commitButton.setPreferredSize(new Dimension(250, 50));
		commitButton.setFont(commitButton.getFont().deriveFont(20.0f));
		commitButton.addActionListener(event -> {
			Match match = new Match();

			switch (buttonGroup.getSelection().getActionCommand()) {
				case "Dodaj":
					if (team1TextField.getText().equals("") || team2TextField.getText().equals("") || homeTeamTextField.getText().equals("")
							|| matchTimeTextField.getText().equals("") || leagueTextField.getText().equals("") || roundTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Błąd", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							match.setTeam1(team1TextField.getText());
							match.setTeam2(team2TextField.getText());
							match.setHomeTeam(homeTeamTextField.getText());
							match.setMatchTime(Date.valueOf(matchTimeTextField.getText()));
							match.setLeague(leagueTextField.getText());
							if (!shootsTeam1TextField.getText().equals("")) {
								match.setShootsTeam1(Integer.valueOf(shootsTeam1TextField.getText()));
							}
							if (!shootsTeam2TeamTextField.getText().equals("")) {
								match.setShootsTeam2(Integer.valueOf(shootsTeam2TeamTextField.getText()));
							}
							if (!possessionTeam1TextField.getText().equals("")) {
								match.setPossessionTeam1(Integer.valueOf(possessionTeam1TextField.getText()));
							}
							if (!possessionTeam2TextField.getText().equals("")) {
								match.setPossessionTeam2(Integer.valueOf(possessionTeam2TextField.getText()));
							}
							if (!POTMTextField.getText().equals("")) {
								match.setPOTM(Integer.valueOf(POTMTextField.getText()));
							}
							match.setRound(Integer.valueOf(roundTextField.getText()));
							matchDAO.addMatch(match);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						} catch (IllegalArgumentException e ){
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}

					}

					break;
				case "Usun":
					if (team1TextField.getText().equals("") || team2TextField.getText().equals("") || homeTeamTextField.getText().equals("")
							|| matchTimeTextField.getText().equals("") || leagueTextField.getText().equals("") || roundTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							match.setTeam1(team1TextField.getText());
							match.setTeam2(team2TextField.getText());
							match.setHomeTeam(homeTeamTextField.getText());
							match.setMatchTime(Date.valueOf(matchTimeTextField.getText()));
							match.setLeague(leagueTextField.getText());
							if (!shootsTeam1TextField.getText().equals("")) {
								match.setShootsTeam1(Integer.valueOf(shootsTeam1TextField.getText()));
							}
							if (!shootsTeam2TeamTextField.getText().equals("")) {
								match.setShootsTeam2(Integer.valueOf(shootsTeam2TeamTextField.getText()));
							}
							if (!possessionTeam1TextField.getText().equals("")) {
								match.setPossessionTeam1(Integer.valueOf(possessionTeam1TextField.getText()));
							}
							if (!possessionTeam2TextField.getText().equals("")) {
								match.setPossessionTeam2(Integer.valueOf(possessionTeam2TextField.getText()));
							}
							if (!POTMTextField.getText().equals("")) {
								match.setPOTM(Integer.valueOf(POTMTextField.getText()));
							}
							match.setRound(Integer.valueOf(roundTextField.getText()));
							matchDAO.removeMatch(match);
						} catch (NumberFormatException e ) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						} catch (IllegalArgumentException e ){
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}
					}
					break;
				default:
					if (team1TextField.getText().equals("") || team2TextField.getText().equals("") || homeTeamTextField.getText().equals("")
							|| matchTimeTextField.getText().equals("") || leagueTextField.getText().equals("") || roundTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							match.setTeam1(team1TextField.getText());
							match.setTeam2(team2TextField.getText());
							match.setHomeTeam(homeTeamTextField.getText());
							match.setMatchTime(Date.valueOf(matchTimeTextField.getText()));
							match.setRound(Integer.valueOf(roundTextField.getText()));
							match.setLeague(leagueTextField.getText());
							if (team1UpdatedTextField.getText().equals(""))
								team1UpdatedTextField.setText(team1TextField.getText());
							if (team2UpdatedTextField.getText().equals(""))
								team2UpdatedTextField.setText(team2TextField.getText());
							if (homeUpdatedTeamTextField.getText().equals(""))
								homeUpdatedTeamTextField.setText(homeTeamTextField.getText());
							if (matchUpdatedTimeTextField.getText().equals(""))
								matchUpdatedTimeTextField.setText(matchTimeTextField.getText());
							if (leagueUpdatedTextField.getText().equals(""))
								leagueUpdatedTextField.setText(leagueTextField.getText());
							if (shootsTeam1UpdatedTextField.getText().equals(""))
								shootsTeam1UpdatedTextField.setText(shootsTeam1TextField.getText());
							if (shootsTeam2UpdatedTeamTextField.getText().equals(""))
								shootsTeam2UpdatedTeamTextField.setText(shootsTeam2TeamTextField.getText());
							if (possessionTeam1UpdatedTextField.getText().equals(""))
								possessionTeam1UpdatedTextField.setText(possessionTeam1TextField.getText());
							if (possessionTeam2UpdatedTextField.getText().equals(""))
								possessionTeam2UpdatedTextField.setText(possessionTeam2TextField.getText());
							if (POTMTextUpdatedField.getText().equals(""))
								POTMTextUpdatedField.setText(POTMTextField.getText());
							if (roundTextUpdatedField.getText().equals(""))
								roundTextUpdatedField.setText(roundTextField.getText());

							Match matchUpdated = new Match();
							matchUpdated.setTeam1(team1TextField.getText());
							matchUpdated.setTeam2(team2TextField.getText());
							matchUpdated.setHomeTeam(homeTeamTextField.getText());
							matchUpdated.setMatchTime(Date.valueOf(matchTimeTextField.getText()));
							matchUpdated.setLeague(leagueTextField.getText());
							if (!shootsTeam1TextField.getText().equals("")) {
								matchUpdated.setShootsTeam1(Integer.valueOf(shootsTeam1TextField.getText()));
							}
							if (!shootsTeam2TeamTextField.getText().equals("")) {
								matchUpdated.setShootsTeam2(Integer.valueOf(shootsTeam2TeamTextField.getText()));
							}
							if (!possessionTeam1TextField.getText().equals("")) {
								matchUpdated.setPossessionTeam1(Integer.valueOf(possessionTeam1TextField.getText()));
							}
							if (!possessionTeam2TextField.getText().equals("")) {
								matchUpdated.setPossessionTeam2(Integer.valueOf(possessionTeam2TextField.getText()));
							}
							if (!POTMTextField.getText().equals("")) {
								matchUpdated.setPOTM(Integer.valueOf(POTMTextField.getText()));
							}
							matchUpdated.setRound(Integer.valueOf(roundTextField.getText()));
							matchDAO.updateMatch(match, matchUpdated);
						} catch (NumberFormatException e ) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						} catch (IllegalArgumentException e ){
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}
					}
					break;
			}
			this.dispose();
		});
		acceptingButtonPanel.add(commitButton);

		JButton showButton = new JButton("Pokaz mecze");
		showButton.setPreferredSize(new Dimension(250, 50));
		showButton.setFont(commitButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("ID | Drużyna 1 | Druzyna 2 | Gospodarz | Data meczu | Liga | Strzały Drużyny 1 | Strzały Drużyny 2 | " +
					"Posiadanie Piłki 1 | Posiadanie Piłki 2 | POTM | Runda\n");
			ArrayList<Match> matchList = null;
			Match m = new Match();
			if (team1TextField.getText().equals("") && team2TextField.getText().equals("") && homeTeamTextField.getText().equals("")
					&& matchTimeTextField.getText().equals("") && leagueTextField.getText().equals("") && shootsTeam1TextField.getText().equals("")
					&& shootsTeam2TeamTextField.getText().equals("") && possessionTeam1TextField.getText().equals("") && possessionTeam2TextField.getText().equals("")
					&& POTMTextField.getText().equals("") && roundTextField.getText().equals("")) {
				matchList = matchDAO.getAllMatches();
			} else {
				try {
					if (!team1TextField.equals("")) {
						m.setTeam1(team1TextField.getText());
					}
					if (!team2TextField.getText().equals("")) {
						m.setTeam2(team2TextField.getText());
					}
					if (!homeTeamTextField.getText().equals("")) {
						m.setHomeTeam(homeTeamTextField.getText());
					}
					if (!matchTimeTextField.getText().equals("")) {
						m.setMatchTime(Date.valueOf(matchTimeTextField.getText()));
					}
					if (!leagueTextField.getText().equals("")) {
						m.setLeague(leagueTextField.getText());
					}
					if (!shootsTeam1TextField.getText().equals("")) {
						m.setShootsTeam1(Integer.valueOf(shootsTeam1TextField.getText()));
					}
					if (!shootsTeam2TeamTextField.getText().equals("")) {
						m.setShootsTeam2(Integer.valueOf(shootsTeam2TeamTextField.getText()));
					}
					if (!possessionTeam1TextField.getText().equals("")) {
						m.setPossessionTeam1(Integer.valueOf(possessionTeam1TextField.getText()));
					}
					if (!possessionTeam2TextField.getText().equals("")) {
						m.setPossessionTeam2(Integer.valueOf(possessionTeam2TextField.getText()));
					}
					if (!POTMTextField.getText().equals("")) {
						m.setPOTM(Integer.valueOf(POTMTextField.getText()));
					}
					if (!roundTextField.getText().equals("")) {
						m.setRound(Integer.valueOf(roundTextField.getText()));
					}
					matchList = matchDAO.getMatches(m);
				} catch (NumberFormatException e ) {
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				} catch (IllegalArgumentException e ){
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				}
			}

			if (matchList != null) {
				for (Match match : matchList) {
					String stringBuilder = match.getMatchID() + " | " + (match.getTeam1() + " | ") +
							match.getTeam2() + " | " + match.getHomeTeam() + " | " + match.getMatchTime() + " | " +
							match.getLeague() + " | " + match.getShootsTeam1() + " | " + match.getShootsTeam2() + " | " +
							match.getPossessionTeam1() + " | " + match.getPossessionTeam2() + " | " + match.getPOTM() + " | " +
							match.getRound() + "\n";
					((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);

		JButton addCardB = new JButton("Dodaj kartke");
		addCardB.addActionListener(e -> {
			CardDialog cardDialog = new CardDialog((JDialog) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
			cardDialog.setVisible(true);
		});
		addCardB.setPreferredSize(new Dimension(250, 50));
		addCardB.setFont(commitButton.getFont().deriveFont(20.0f));
		acceptingButtonPanel.add(addCardB);
		JButton addGoalB = new JButton("Dodaj Gola/Asyste");
		addGoalB.addActionListener(e -> {
			GoalsAssistsDialog goalsAssistsDialog = new GoalsAssistsDialog((JDialog) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
			goalsAssistsDialog.setVisible(true);
		});
		addGoalB.setPreferredSize(new Dimension(250, 50));
		addGoalB.setFont(commitButton.getFont().deriveFont(20.0f));
		acceptingButtonPanel.add(addGoalB);
		JButton addSquadB = new JButton("Dodaj Skład");
		addSquadB.addActionListener(e -> {
			SquadDialog squadDialog = new SquadDialog((JDialog) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
			squadDialog.setVisible(true);
		});
		addSquadB.setPreferredSize(new Dimension(250, 50));
		addSquadB.setFont(commitButton.getFont().deriveFont(20.0f));
		acceptingButtonPanel.add(addSquadB);
	}

	protected void switchButtons(boolean b) {
		team1UpdatedTextField.setEditable(b);
		team2UpdatedTextField.setEditable(b);
		homeUpdatedTeamTextField.setEditable(b);
		matchUpdatedTimeTextField.setEditable(b);
		leagueUpdatedTextField.setEditable(b);
		shootsTeam1UpdatedTextField.setEditable(b);
		shootsTeam2UpdatedTeamTextField.setEditable(b);
		possessionTeam1UpdatedTextField.setEditable(b);
		possessionTeam2UpdatedTextField.setEditable(b);
		POTMTextUpdatedField.setEditable(b);
		roundTextUpdatedField.setEditable(b);
	}

	public JTextArea getDisplay() {
		return display;
	}
}
