package View;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {
	private JPanel buttonPanel;
	private JTextArea display;
	private JScrollPane scrollPane;
	private JButton clubB;
	private JButton footballerB;
	private JButton stadiumB;
	private JButton coachB;
	private JButton matchB;
	private JButton leagueB;
	private JButton backupB;
	private JButton restoreB;
	private JButton addUserB;
	private JButton roundB;
	private JButton kingGoalsB;
	private JButton kingAssists;
	private JTextField timer;

	MainPanel(){
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(14, 1, 10, 20));
		buttonPanel.setPreferredSize(new Dimension(225, 750));

		clubB = new JButton("Kluby");
		footballerB = new JButton("Piłkarze");
		stadiumB = new JButton("Stadiony");
		coachB = new JButton("Trenerzy");
		matchB = new JButton("Mecze");
		leagueB = new JButton("Ligi");
		roundB = new JButton("Runda");
		kingGoalsB = new JButton("Król Strzelców");
		kingAssists = new JButton("Najwięcej stworzonych okazji");

		buttonPanel.add(footballerB);
		buttonPanel.add(clubB);
		buttonPanel.add(matchB);
		buttonPanel.add(coachB);
		buttonPanel.add(stadiumB);
		buttonPanel.add(leagueB);
		buttonPanel.add(roundB);
		buttonPanel.add(kingGoalsB);
		buttonPanel.add(kingAssists);


		display = new JTextArea();
		display.setEditable(false);
		display.setFont(display.getFont().deriveFont(24.0f));
		scrollPane = new JScrollPane(display);
		scrollPane.setPreferredSize(new Dimension(new Dimension(950, 750)));

		add(buttonPanel);
		add(scrollPane);
	}

	public JButton getClubB() {
		return clubB;
	}

	public JButton getFootballerB() {
		return footballerB;
	}

	public JButton getStadiumB() {
		return stadiumB;
	}

	public JButton getCoachB() {
		return coachB;
	}

	public JButton getMatchB() {
		return matchB;
	}

	public JButton getLeagueB() {
		return leagueB;
	}

	public JTextArea getDisplay() {
		return display;
	}

	public JButton getBackupB() {
		return backupB;
	}

	public JButton getRestoreB() {
		return restoreB;
	}

	public JButton getAddUserB() {
		return addUserB;
	}

	public void addAdminButtons(boolean admin, String favTeam){
		backupB = new JButton("Backup");
		restoreB = new JButton("Przywróć BD");
		addUserB = new JButton("Dodaj użytkownika");
		JLabel favLabel = new JLabel("Next match "+favTeam);
		timer = new JTextField(20);
		timer.setEditable(false);

		buttonPanel.add(favLabel);
		buttonPanel.add(timer);
		if(admin) {
			buttonPanel.add(backupB);
			buttonPanel.add(restoreB);
			buttonPanel.add(addUserB);
		}
	}

	public JTextField getTimer(){
		return timer;
	}

	public JButton getRoundB() {
		return roundB;
	}

	public JButton getKingGoalsB() {
		return kingGoalsB;
	}

	public JButton getKingAssists() {
		return kingAssists;
	}
}
