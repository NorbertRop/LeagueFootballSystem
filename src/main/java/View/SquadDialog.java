package View;

import DAO.SquadDAO;
import DAO.SquadDAOImp;
import Entities.Squad;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class SquadDialog extends JDialog {
	private SquadDAO squadDAO;
	private JTextField footballerTextField;
	private JTextField matchTextField;
	private JTextField startingTextField;

	public SquadDialog(JDialog owner){
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(500, 250);
		setLocation(400, 400);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Zarządzanie składami");
		setLayout(new GridLayout(5, 2, 10, 10));
		squadDAO = new SquadDAOImp();

		footballerTextField = new JTextField();
		startingTextField = new JTextField();
		matchTextField = new JTextField();

		JLabel footballerLabel = new JLabel("ID Piłkarza");
		JLabel matchLabel = new JLabel("ID Meczu");
		JLabel typeLabel = new JLabel("Wyjściowy skład (wpisz 1/0)'");
		JButton addB = new JButton("Dodaj");
		addB.addActionListener(e -> {
			Squad squad = new Squad();
			try{
				squad.setFootballer(Integer.valueOf(footballerTextField.getText()));
				if(startingTextField.getText().equals("1") || startingTextField.getText().equals("0")){
					squad.setStartingEleven(Boolean.getBoolean(startingTextField.getText()));
				} else throw new NumberFormatException();
				squad.setMatch(Integer.valueOf(matchTextField.getText()));
				squadDAO.addSquad(squad);
			} catch(NumberFormatException e1){
				JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
			this.dispose();
		});
		JButton removeB = new JButton("Usuń");
		removeB.addActionListener(e ->{
			Squad squad = new Squad();
			try{
				squad.setFootballer(Integer.valueOf(footballerTextField.getText()));
				if(startingTextField.getText().equals("1") || startingTextField.getText().equals("0")){
					squad.setStartingEleven(Boolean.getBoolean(startingTextField.getText()));
				} else throw new NumberFormatException();
				squad.setMatch(Integer.valueOf(matchTextField.getText()));
				squadDAO.removeSquad(squad);
			} catch(NumberFormatException e1){
				JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
			this.dispose();
		});
		JButton showB = new JButton("Pokaż składy");
		showB.addActionListener(e ->{
			Squad squad = new Squad();
			try{
				if(!footballerTextField.getText().equals("")) {
					squad.setFootballer(Integer.valueOf(footballerTextField.getText()));
				}
				if(startingTextField.getText().equals("1") || startingTextField.getText().equals("0")){
					squad.setStartingEleven(Boolean.getBoolean(startingTextField.getText()));
				} else {
					squad.setStartingEleven(null);
				}
				if(!matchTextField.getText().equals("")) {
					squad.setMatch(Integer.valueOf(matchTextField.getText()));
				}
				ArrayList<Squad> squads = squadDAO.getSquads(squad);
				((MatchDialog)owner).getDisplay().setText("Piłkarz | Wyjściowy Skład | Match \n");
				for (Squad c : squads) {
					String stringBuilder = c.getFootballer() + " | " + c.isStartingEleven() + " | " +
							c.getMatch() + "\n";
					((MatchDialog)owner).getDisplay().setText(((MatchDialog)owner).getDisplay().getText() + stringBuilder);
				}
			} catch(NumberFormatException e1){
				JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
			this.dispose();
		});
		add(footballerLabel);
		add(footballerTextField);
		add(matchLabel);
		add(matchTextField);
		add(typeLabel);
		add(startingTextField);
		add(addB);
		add(removeB);
		add(showB);
	}
}
