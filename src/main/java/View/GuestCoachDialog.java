package View;

import DAO.CoachDAO;
import DAO.CoachDAOImp;
import Entities.Coach;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class GuestCoachDialog extends JDialog {
	private CoachDAO coachDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField nameTextField;
	private JTextField surnameTextField;
	private JTextField clubTextField;
	private JTextField specializationTextField;

	public GuestCoachDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(400, 250);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Zarządzanie trenerem");

		coachDAO = new CoachDAOImp();

		prepareTextFieldPanel();
		prepareAcceptingButtonPanel();

		add(textFieldsPanel);
		add(acceptingButtonPanel);
	}

	private void prepareTextFieldPanel() {
		nameTextField = new JTextField();
		surnameTextField = new JTextField();
		clubTextField = new JTextField();
		specializationTextField = new JTextField();

		JLabel nameLabel = new JLabel("Imie");
		JLabel surnameLabel = new JLabel("Nazwisko");
		JLabel clubLabel = new JLabel("Klub");
		JLabel specializationLabel = new JLabel("Specjalizacja");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(4, 2, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(300, 150));

		textFieldsPanel.add(nameLabel);
		textFieldsPanel.add(nameTextField);

		textFieldsPanel.add(surnameLabel);
		textFieldsPanel.add(surnameTextField);

		textFieldsPanel.add(clubLabel);
		textFieldsPanel.add(clubTextField);

		textFieldsPanel.add(specializationLabel);
		textFieldsPanel.add(specializationTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(200, 150));

		JButton showButton = new JButton("Pokaz trenerów");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(showButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("Imie | Nazwisko | Klub | Specjalizacja\n");
			ArrayList<Coach> coachList;
			Coach c = new Coach();
			if (nameTextField.getText().equals("") && surnameTextField.getText().equals("") && clubTextField.getText().equals("") && specializationTextField.getText().equals("")) {
				coachList = coachDAO.getAllCoaches();
			} else {
				if(!nameTextField.equals("")) {
					c.setName(nameTextField.getText());
				}
				if (!surnameTextField.getText().equals("")) {
					c.setSurname(surnameTextField.getText());
				}
				if(!clubTextField.getText().equals("")) {
					c.setClub(clubTextField.getText());
				}
				if(!specializationTextField.getText().equals("")) {
					c.setSpecialization(specializationTextField.getText());
				}
				coachList = coachDAO.getCoaches(c);
			}
			for (Coach coach : coachList) {
				String stringBuilder = (coach.getName() + " | ") +
						coach.getSurname() + " | " +
						coach.getClub() + " | " + coach.getSpecialization() + "\n";
				((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}
}
