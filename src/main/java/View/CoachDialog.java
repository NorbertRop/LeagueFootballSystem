package View;

import DAO.CoachDAO;
import DAO.CoachDAOImp;
import Entities.Coach;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class CoachDialog extends MyDialog {
	private CoachDAO coachDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField nameTextField;
	private JTextField surnameTextField;
	private JTextField clubTextField;
	private JTextField specializationTextField;

	private JTextField nameUpdatedTextField;
	private JTextField surnameUpdatedTextField;
	private JTextField clubUpdatedTextField;
	private JTextField specializationUpdatedTextField;

	public CoachDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(750, 500);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Zarządzanie trenerem");

		coachDAO = new CoachDAOImp();

		prepareTextFieldPanel();
		prepareRadioBox();
		prepareAcceptingButtonPanel();

		add(radioBoxPanel);
		add(textFieldsPanel);
		add(acceptingButtonPanel);
	}

	private void prepareTextFieldPanel() {
		nameTextField = new JTextField();
		surnameTextField = new JTextField();
		clubTextField = new JTextField();
		specializationTextField = new JTextField();

		nameUpdatedTextField = new JTextField();
		nameUpdatedTextField.setEditable(false);
		surnameUpdatedTextField = new JTextField();
		surnameUpdatedTextField.setEditable(false);
		clubUpdatedTextField = new JTextField();
		clubUpdatedTextField.setEditable(false);
		specializationUpdatedTextField = new JTextField();
		specializationUpdatedTextField.setEditable(false);

		JLabel nameLabel = new JLabel("Imie");
		JLabel surnameLabel = new JLabel("Nazwisko");
		JLabel clubLabel = new JLabel("Klub");
		JLabel specializationLabel = new JLabel("Specjalizacja");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(4, 3, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(300, 150));

		textFieldsPanel.add(nameLabel);
		textFieldsPanel.add(nameTextField);
		textFieldsPanel.add(nameUpdatedTextField);

		textFieldsPanel.add(surnameLabel);
		textFieldsPanel.add(surnameTextField);
		textFieldsPanel.add(surnameUpdatedTextField);

		textFieldsPanel.add(clubLabel);
		textFieldsPanel.add(clubTextField);
		textFieldsPanel.add(clubUpdatedTextField);

		textFieldsPanel.add(specializationLabel);
		textFieldsPanel.add(specializationTextField);
		textFieldsPanel.add(specializationUpdatedTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(200, 150));
		JButton commitButton = new JButton("Zatwierdz");
		commitButton.setPreferredSize(new Dimension(200, 50));
		commitButton.setFont(commitButton.getFont().deriveFont(20.0f));
		commitButton.addActionListener(event -> {
			Coach coach = new Coach();

			switch (buttonGroup.getSelection().getActionCommand()) {
				case "Dodaj":
					if (nameTextField.getText().equals("") || surnameTextField.getText().equals("") || specializationTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Błąd", JOptionPane.ERROR_MESSAGE);
					} else {
						coach.setName(nameTextField.getText());
						coach.setSurname(surnameTextField.getText());
						coach.setClub(clubTextField.getText());
						coach.setSpecialization(specializationTextField.getText());
						coachDAO.addCoach(coach);
					}

					break;
				case "Usun":
					if (nameTextField.getText().equals("") || surnameTextField.getText().equals("") || specializationTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						coach.setName(nameTextField.getText());
						coach.setSurname(surnameTextField.getText());
						coach.setClub(clubTextField.getText());
						coach.setSpecialization(specializationTextField.getText());
						coachDAO.removeCoach(coach);
					}
					break;
				default:
					if (nameTextField.getText().equals("") || surnameTextField.getText().equals("") || specializationTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						coach.setName(nameTextField.getText());
						coach.setSurname(surnameTextField.getText());
						coach.setClub(clubTextField.getText());
						coach.setSpecialization(specializationTextField.getText());
						if (nameUpdatedTextField.getText().equals(""))
							nameUpdatedTextField.setText(nameTextField.getText());
						if (surnameUpdatedTextField.getText().equals(""))
							surnameUpdatedTextField.setText(surnameTextField.getText());
						if (clubUpdatedTextField.getText().equals(""))
							clubUpdatedTextField.setText(clubTextField.getText());
						if (specializationUpdatedTextField.getText().equals(""))
							specializationUpdatedTextField.setText(specializationTextField.getText());

						Coach coachUpdated = new Coach();
						coachUpdated.setName(nameUpdatedTextField.getText());
						coachUpdated.setSurname(surnameUpdatedTextField.getText());
						coachUpdated.setClub(clubUpdatedTextField.getText());
						coachUpdated.setSpecialization(specializationUpdatedTextField.getText());
						coachDAO.updateCoach(coach, coachUpdated);
					}
					break;
			}
			this.dispose();
		});
		acceptingButtonPanel.add(commitButton);

		JButton showButton = new JButton("Pokaz trenerów");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(commitButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("Imie | Nazwisko | Klub | Specjalizacja\n");
			ArrayList<Coach> coachList;
			Coach c = new Coach();
			if (nameTextField.getText().equals("") && surnameTextField.getText().equals("") && clubTextField.getText().equals("") && specializationTextField.getText().equals("")) {
				coachList = coachDAO.getAllCoaches();
			} else {
				if(!nameTextField.equals("")) {
					c.setName(nameTextField.getText());
				}
				if (!surnameTextField.getText().equals("")) {
					c.setSurname(surnameTextField.getText());
				}
				if(!clubTextField.getText().equals("")) {
					c.setClub(clubTextField.getText());
				}
				if(!specializationTextField.getText().equals("")) {
					c.setSpecialization(specializationTextField.getText());
				}
				coachList = coachDAO.getCoaches(c);
			}
			for (Coach coach : coachList) {
				String stringBuilder = (coach.getName() + " | ") +
						coach.getSurname() + " | " +
						coach.getClub() + " | " + coach.getSpecialization() + "\n";
				((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}

	protected void switchButtons(boolean b) {
		nameUpdatedTextField.setEditable(b);
		surnameUpdatedTextField.setEditable(b);
		clubUpdatedTextField.setEditable(b);
		specializationUpdatedTextField.setEditable(b);
	}
}
