package View;

import DAO.StadiumDAO;
import DAO.StadiumDAOImp;
import Entities.Stadium;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class StadiumDialog extends MyDialog {
	private StadiumDAO stadiumDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField nameTextField;
	private JTextField capacityTextField;
	private JTextField cityTextField;


	private JTextField nameUpdatedTextField;
	private JTextField capacityUpdatedTextField;
	private JTextField cityUpdatedTextField;

	public StadiumDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(750, 500);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Zarządzanie stadionami");

		stadiumDAO = new StadiumDAOImp();

		prepareTextFieldPanel();
		prepareRadioBox();
		prepareAcceptingButtonPanel();

		add(radioBoxPanel);
		add(textFieldsPanel);
		add(acceptingButtonPanel);
	}

	private void prepareTextFieldPanel() {
		nameTextField = new JTextField();
		capacityTextField = new JTextField();
		cityTextField = new JTextField();

		nameUpdatedTextField = new JTextField();
		nameUpdatedTextField.setEditable(false);
		capacityUpdatedTextField = new JTextField();
		capacityUpdatedTextField.setEditable(false);
		cityUpdatedTextField = new JTextField();
		cityUpdatedTextField.setEditable(false);

		JLabel nameLabel = new JLabel("Nazwa");
		JLabel capacityLabel = new JLabel("Pojemność");
		JLabel cityLabel = new JLabel("Miasto");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(4, 3, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(300, 150));

		textFieldsPanel.add(nameLabel);
		textFieldsPanel.add(nameTextField);
		textFieldsPanel.add(nameUpdatedTextField);

		textFieldsPanel.add(capacityLabel);
		textFieldsPanel.add(capacityTextField);
		textFieldsPanel.add(capacityUpdatedTextField);

		textFieldsPanel.add(cityLabel);
		textFieldsPanel.add(cityTextField);
		textFieldsPanel.add(cityUpdatedTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(200, 150));
		JButton commitButton = new JButton("Zatwierdz");
		commitButton.setPreferredSize(new Dimension(200, 50));
		commitButton.setFont(commitButton.getFont().deriveFont(20.0f));
		commitButton.addActionListener(event -> {
			Stadium stadium = new Stadium();

			switch (buttonGroup.getSelection().getActionCommand()) {
				case "Dodaj":
					if (nameTextField.getText().equals("") || capacityTextField.getText().equals("") || cityTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Błąd", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							stadium.setName(nameTextField.getText());
							stadium.setCapacity(Integer.valueOf(capacityTextField.getText()));
							stadium.setCity(cityTextField.getText());
							stadiumDAO.addStadium(stadium);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}
					}

					break;
				case "Usun":
					if (nameTextField.getText().equals("") || capacityTextField.getText().equals("") || cityTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							stadium.setName(nameTextField.getText());
							stadium.setCapacity(Integer.valueOf(capacityTextField.getText()));
							stadium.setCity(cityTextField.getText());
							stadiumDAO.removeStadium(stadium);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}
					}
					break;
				default:
					if (nameTextField.getText().equals("") || capacityTextField.getText().equals("") || cityTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							stadium.setName(nameTextField.getText());
							stadium.setCapacity(Integer.valueOf(capacityTextField.getText()));
							stadium.setCity(cityTextField.getText());
							if (nameUpdatedTextField.getText().equals(""))
								nameUpdatedTextField.setText(nameTextField.getText());
							if (capacityUpdatedTextField.getText().equals(""))
								capacityUpdatedTextField.setText(capacityTextField.getText());
							if (cityUpdatedTextField.getText().equals(""))
								cityUpdatedTextField.setText(cityTextField.getText());

							Stadium coachUpdated = new Stadium();
							coachUpdated.setName(nameUpdatedTextField.getText());
							coachUpdated.setCapacity(Integer.valueOf(capacityUpdatedTextField.getText()));
							coachUpdated.setCity(cityUpdatedTextField.getText());
							stadiumDAO.updateStadium(stadium, coachUpdated);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}
					}
					break;
			}
			this.dispose();
		});
		acceptingButtonPanel.add(commitButton);

		JButton showButton = new JButton("Pokaz stadiony");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(commitButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("Nazwa | Pojemność | Miasto \n");
			Stadium s = new Stadium();
			ArrayList<Stadium> stadiumList = null;
			if (nameTextField.getText().equals("") && capacityTextField.getText().equals("") && cityTextField.getText().equals("")) {
				stadiumList = stadiumDAO.getAllStadiums();
			} else {
				try {
					if (!nameTextField.equals("")) {
						s.setName(nameTextField.getText());
					}
					if (!capacityTextField.getText().equals("")) {
						s.setCapacity(Integer.valueOf(capacityTextField.getText()));
					}
					if (!cityTextField.getText().equals("")) {
						s.setCity(cityTextField.getText());
					}
					stadiumList = stadiumDAO.getStadiums(s);
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				}
			}

			if (stadiumList != null) {
				for (Stadium stadium : stadiumList) {
					String stringBuilder = (stadium.getName() + " | ") +
							stadium.getCapacity() + " | " +
							stadium.getCity() + "\n";
					((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}

	protected void switchButtons(boolean b) {
		nameUpdatedTextField.setEditable(b);
		capacityUpdatedTextField.setEditable(b);
		cityUpdatedTextField.setEditable(b);
	}
}
