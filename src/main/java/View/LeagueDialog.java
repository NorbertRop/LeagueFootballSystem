package View;

import DAO.LeagueDAO;
import DAO.LeagueDAOImp;
import Entities.League;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class LeagueDialog extends MyDialog {
	private LeagueDAO leagueDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField nameTextField;
	private JTextField nameUpdatedTextField;

	public LeagueDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(750, 350);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Zarządzanie ligą");

		leagueDAO = new LeagueDAOImp();

		prepareTextFieldPanel();
		prepareRadioBox();
		prepareAcceptingButtonPanel();

		add(radioBoxPanel);
		add(textFieldsPanel);
		add(acceptingButtonPanel);
	}

	private void prepareTextFieldPanel() {
		nameTextField = new JTextField(15);

		nameUpdatedTextField = new JTextField(15);
		nameUpdatedTextField.setEditable(false);

		JLabel nameLabel = new JLabel("Nazwa");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(4, 3, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(300, 150));
		textFieldsPanel.add(nameLabel);
		textFieldsPanel.add(nameTextField);
		textFieldsPanel.add(nameUpdatedTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(200, 150));
		JButton commitButton = new JButton("Zatwierdz");
		commitButton.setPreferredSize(new Dimension(200, 50));
		commitButton.setFont(commitButton.getFont().deriveFont(20.0f));
		commitButton.addActionListener(event -> {
			League league = new League();

			switch (buttonGroup.getSelection().getActionCommand()) {
				case "Dodaj":
					if (nameTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Błąd", JOptionPane.ERROR_MESSAGE);
					} else {
						league.setName(nameTextField.getText());
						leagueDAO.addLeague(league);
					}

					break;
				case "Usun":
					if (nameTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						league.setName(nameTextField.getText());
						leagueDAO.removeLeague(league);
					}
					break;
				default:
					if (nameTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						league.setName(nameTextField.getText());
						if (!nameUpdatedTextField.getText().equals("")) {
							League leagueUpdated = new League();
							leagueUpdated.setName(nameUpdatedTextField.getText());
							leagueDAO.updateLeague(league, leagueUpdated);
						}
					}
					break;
			}
			this.dispose();
		});
		acceptingButtonPanel.add(commitButton);

		JButton showButton = new JButton("Pokaz ligi");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(commitButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("Nazwa\n");
			League l = new League();
			ArrayList<League> leagueList;
			if (nameTextField.getText().equals("")) {
				leagueList = leagueDAO.getAllLeagues();
			} else {
				if (!nameTextField.getText().equals("")) {
					l.setName(nameTextField.getText());
				}
				leagueList = leagueDAO.getLeagues(l);
			}
			for (League league : leagueList) {
				String stringBuilder = league.getName() + "\n";
				((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}

	@Override
	protected void switchButtons(boolean b) {
		nameUpdatedTextField.setEditable(b);
	}
}
