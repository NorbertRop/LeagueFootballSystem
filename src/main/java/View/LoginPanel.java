package View;

import javax.swing.*;

public class LoginPanel extends JPanel{
	private JButton loginB;
	private JButton guestB;
	private JTextField loginField;
	private JPasswordField passwordField;

	LoginPanel(){
		loginB = new JButton("Login");
		guestB = new JButton("Login as Guest");
		loginField = new JTextField(15);
		passwordField = new JPasswordField(15);

		add(loginField);
		add(passwordField);
		add(loginB);
		add(guestB);
	}

	public JButton getLoginB() {
		return loginB;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public JTextField getLoginField() {
		return loginField;
	}

	public JButton getGuestB() {
		return guestB;
	}
}
