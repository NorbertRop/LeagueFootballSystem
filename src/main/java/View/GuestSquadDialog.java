package View;

import DAO.SquadDAO;
import DAO.SquadDAOImp;
import Entities.Squad;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class GuestSquadDialog extends JDialog {
	private SquadDAO squadDAO;
	private JTextField footballerTextField;
	private JTextField matchTextField;
	private JTextField startingTextField;

	public GuestSquadDialog(JDialog owner){
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(500, 250);
		setLocation(400, 400);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Zarządzanie składami");
		setLayout(new GridLayout(4, 2, 10, 10));
		squadDAO = new SquadDAOImp();

		footballerTextField = new JTextField();
		startingTextField = new JTextField();
		matchTextField = new JTextField();

		JLabel footballerLabel = new JLabel("ID Piłkarza");
		JLabel matchLabel = new JLabel("ID Meczu");
		JLabel typeLabel = new JLabel("Wyjściowy skład (wpisz 1/0)'");

		JButton showB = new JButton("Pokaż składy");
		showB.addActionListener(e ->{
			Squad squad = new Squad();
			try{
				if(!footballerTextField.getText().equals("")) {
					squad.setFootballer(Integer.valueOf(footballerTextField.getText()));
				}
				if(startingTextField.getText().equals("1") || startingTextField.getText().equals("0")){
					squad.setStartingEleven(Boolean.getBoolean(startingTextField.getText()));
				} else {
					squad.setStartingEleven(null);
				}
				if(!matchTextField.getText().equals("")) {
					squad.setMatch(Integer.valueOf(matchTextField.getText()));
				}
				ArrayList<Squad> squads = squadDAO.getSquads(squad);
				((GuestMatchDialog)owner).getDisplay().setText("Piłkarz | Wyjściowy Skład | Match \n");
				for (Squad c : squads) {
					String stringBuilder = c.getFootballer() + " | " + c.isStartingEleven() + " | " +
							c.getMatch() + "\n";
					((GuestMatchDialog)owner).getDisplay().setText(((GuestMatchDialog)owner).getDisplay().getText() + stringBuilder);
				}
			} catch(NumberFormatException e1){
				JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
			this.dispose();
		});
		add(footballerLabel);
		add(footballerTextField);
		add(matchLabel);
		add(matchTextField);
		add(typeLabel);
		add(startingTextField);
		add(showB);
	}
}
