package View;

import DBUtils.DBConnection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ClientView extends JFrame {
	private LoginPanel loginPanel;
	private MainPanel mainPanel;

	public ClientView(){
		loginPanel = new LoginPanel();
		add(loginPanel);

		setTitle("LeagueFootballSystemLogin");
		setLocation(400, 400);
		setSize(new Dimension(400, 100));
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				DBConnection.closeConnection();
				System.exit(0);
			}
		});
	}

	public LoginPanel getLoginPanel() {
		return loginPanel;
	}

	public void setMainPanel(){
		mainPanel = new MainPanel();
		getContentPane().removeAll();
		add(mainPanel);
		setLocation(100, 100);
		validate();
		repaint();
	}

	public MainPanel getMainPanel() {
		return mainPanel;
	}
}
