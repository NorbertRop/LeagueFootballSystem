package View;

import DAO.ClubDAO;
import DAO.ClubDAOImp;
import Entities.Club;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class GuestClubDialog extends JDialog{
	private ClubDAO clubDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField fullNameTextField;
	private JTextField stadiumTextField;
	private JTextField goalsBalanceTextField;
	private JTextField winsTextField;
	private JTextField lossesTextField;
	private JTextField drawsTextField;
	private JTextField pointsTextField;
	private JTextField leagueTextField;

	public GuestClubDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(800, 350);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Zarządzanie klubem");

		clubDAO = new ClubDAOImp();

		prepareTextFieldPanel();
		prepareAcceptingButtonPanel();

		add(textFieldsPanel);
		add(acceptingButtonPanel);
	}

	private void prepareTextFieldPanel() {
		fullNameTextField = new JTextField();
		stadiumTextField = new JTextField();
		goalsBalanceTextField = new JTextField();
		winsTextField = new JTextField();
		lossesTextField = new JTextField();
		drawsTextField = new JTextField();
		pointsTextField = new JTextField();
		leagueTextField = new JTextField();

		JLabel fullNameLabel = new JLabel("Pełna nazwa");
		JLabel stadiumLabel = new JLabel("Stadion");
		JLabel goalsBalanceLabel = new JLabel("Bilans Goli");
		JLabel winsLabel = new JLabel("Wygrane");
		JLabel lossesLabel = new JLabel("Przegrane");
		JLabel drawsLabel = new JLabel("Remisy");
		JLabel pointsLabel = new JLabel("Punkty");
		JLabel leagueLabel = new JLabel("Liga");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(8, 3, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(500, 300));

		textFieldsPanel.add(fullNameLabel);
		textFieldsPanel.add(fullNameTextField);

		textFieldsPanel.add(stadiumLabel);
		textFieldsPanel.add(stadiumTextField);

		textFieldsPanel.add(goalsBalanceLabel);
		textFieldsPanel.add(goalsBalanceTextField);

		textFieldsPanel.add(winsLabel);
		textFieldsPanel.add(winsTextField);

		textFieldsPanel.add(lossesLabel);
		textFieldsPanel.add(lossesTextField);

		textFieldsPanel.add(drawsLabel);
		textFieldsPanel.add(drawsTextField);

		textFieldsPanel.add(pointsLabel);
		textFieldsPanel.add(pointsTextField);

		textFieldsPanel.add(leagueLabel);
		textFieldsPanel.add(leagueTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(200, 150));

		JButton showButton = new JButton("Pokaz kluby");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(showButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			ArrayList<Club> clubList = null;
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("Pełna Nazwa | Bilans Goli | Wygrane | Przegrane | Remisy | Punkty | Liga \n");
			if (fullNameTextField.getText().equals("") && stadiumTextField.getText().equals("") && goalsBalanceTextField.getText().equals("")
					&& winsTextField.getText().equals("") && lossesTextField.getText().equals("") && drawsTextField.getText().equals("")
					&& pointsTextField.getText().equals("") && leagueTextField.getText().equals("")) {
				clubList = clubDAO.getAllClubs();
			} else {
				try {
					Club club = new Club();
					if (!fullNameTextField.getText().equals(""))
						club.setFullName(fullNameTextField.getText());
					if (!stadiumTextField.getText().equals(""))
						club.setStadium(stadiumTextField.getText());
					if (!goalsBalanceTextField.getText().equals(""))
						club.setGoalsBalance(Integer.valueOf(goalsBalanceTextField.getText()));
					if (!winsTextField.getText().equals(""))
						club.setWins(Integer.valueOf(winsTextField.getText()));
					if (!lossesTextField.getText().equals(""))
						club.setLosses(Integer.valueOf(lossesTextField.getText()));
					if (!drawsTextField.getText().equals(""))
						club.setDraws(Integer.valueOf(drawsTextField.getText()));
					if (!pointsTextField.getText().equals(""))
						club.setPoints(Integer.valueOf(pointsTextField.getText()));
					if (!leagueTextField.getText().equals(""))
						club.setLeague(leagueTextField.getText());
					clubList = clubDAO.getClubs(club);
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				}
			}

			if (clubList != null) {
				for (Club club : clubList) {
					String stringBuilder = (club.getFullName() + " | ") +
							club.getStadium() + " | " + club.getGoalsBalance() + " | " + club.getWins() + " | " +
							club.getLosses() + " | " + club.getDraws() + " | " + club.getPoints() + " | " +
							club.getLeague() + "\n";
					((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}
}
