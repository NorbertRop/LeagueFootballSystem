package View;

import DAO.ClubDAO;
import DAO.ClubDAOImp;
import Entities.Club;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class ClubDialog extends MyDialog {
	private ClubDAO clubDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField fullNameTextField;
	private JTextField stadiumTextField;
	private JTextField goalsBalanceTextField;
	private JTextField winsTextField;
	private JTextField lossesTextField;
	private JTextField drawsTextField;
	private JTextField pointsTextField;
	private JTextField leagueTextField;

	private JTextField fullNameUpdatedTextField;
	private JTextField stadiumUpdatedTextField;
	private JTextField goalsBalanceUpdatedTextField;
	private JTextField winsUpdatedTextField;
	private JTextField lossesUpdatedTextField;
	private JTextField drawsUpdatedTextField;
	private JTextField pointsUpdatedTextField;
	private JTextField leagueUpdatedTextField;

	public ClubDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(750, 500);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Zarządzanie klubem");

		clubDAO = new ClubDAOImp();

		prepareTextFieldPanel();
		prepareRadioBox();
		prepareAcceptingButtonPanel();

		add(radioBoxPanel);
		add(textFieldsPanel);
		add(acceptingButtonPanel);
	}

	private void prepareTextFieldPanel() {
		fullNameTextField = new JTextField();
		stadiumTextField = new JTextField();
		goalsBalanceTextField = new JTextField();
		winsTextField = new JTextField();
		lossesTextField = new JTextField();
		drawsTextField = new JTextField();
		pointsTextField = new JTextField();
		leagueTextField = new JTextField();


		fullNameUpdatedTextField = new JTextField();
		fullNameUpdatedTextField.setEditable(false);
		stadiumUpdatedTextField = new JTextField();
		stadiumUpdatedTextField.setEditable(false);
		goalsBalanceUpdatedTextField = new JTextField();
		goalsBalanceUpdatedTextField.setEditable(false);
		winsUpdatedTextField = new JTextField();
		winsUpdatedTextField.setEditable(false);
		lossesUpdatedTextField = new JTextField();
		lossesUpdatedTextField.setEditable(false);
		drawsUpdatedTextField = new JTextField();
		drawsUpdatedTextField.setEditable(false);
		pointsUpdatedTextField = new JTextField();
		pointsUpdatedTextField.setEditable(false);
		leagueUpdatedTextField = new JTextField();
		leagueUpdatedTextField.setEditable(false);

		JLabel fullNameLabel = new JLabel("Pełna nazwa");
		JLabel stadiumLabel = new JLabel("Stadion");
		JLabel goalsBalanceLabel = new JLabel("Bilans Goli");
		JLabel winsLabel = new JLabel("Wygrane");
		JLabel lossesLabel = new JLabel("Przegrane");
		JLabel drawsLabel = new JLabel("Remisy");
		JLabel pointsLabel = new JLabel("Punkty");
		JLabel leagueLabel = new JLabel("Liga");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(8, 3, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(400, 300));

		textFieldsPanel.add(fullNameLabel);
		textFieldsPanel.add(fullNameTextField);
		textFieldsPanel.add(fullNameUpdatedTextField);

		textFieldsPanel.add(stadiumLabel);
		textFieldsPanel.add(stadiumTextField);
		textFieldsPanel.add(stadiumUpdatedTextField);

		textFieldsPanel.add(goalsBalanceLabel);
		textFieldsPanel.add(goalsBalanceTextField);
		textFieldsPanel.add(goalsBalanceUpdatedTextField);

		textFieldsPanel.add(winsLabel);
		textFieldsPanel.add(winsTextField);
		textFieldsPanel.add(winsUpdatedTextField);

		textFieldsPanel.add(lossesLabel);
		textFieldsPanel.add(lossesTextField);
		textFieldsPanel.add(lossesUpdatedTextField);

		textFieldsPanel.add(drawsLabel);
		textFieldsPanel.add(drawsTextField);
		textFieldsPanel.add(drawsUpdatedTextField);

		textFieldsPanel.add(pointsLabel);
		textFieldsPanel.add(pointsTextField);
		textFieldsPanel.add(pointsUpdatedTextField);

		textFieldsPanel.add(leagueLabel);
		textFieldsPanel.add(leagueTextField);
		textFieldsPanel.add(leagueUpdatedTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(200, 150));
		JButton commitButton = new JButton("Zatwierdz");
		commitButton.setPreferredSize(new Dimension(200, 50));
		commitButton.setFont(commitButton.getFont().deriveFont(20.0f));
		commitButton.addActionListener(event -> {
			Club club = new Club();
			switch (buttonGroup.getSelection().getActionCommand()) {
				case "Dodaj":
					if (fullNameTextField.getText().equals("") || stadiumTextField.getText().equals("") || goalsBalanceTextField.getText().equals("")
							|| winsTextField.getText().equals("") || lossesTextField.getText().equals("") || drawsTextField.getText().equals("")
							|| pointsTextField.getText().equals("") || leagueTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Błąd", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							club.setFullName(fullNameTextField.getText());
							club.setStadium(stadiumTextField.getText());
							club.setGoalsBalance(Integer.valueOf(goalsBalanceTextField.getText()));
							club.setWins(Integer.valueOf(winsTextField.getText()));
							club.setLosses(Integer.valueOf(lossesTextField.getText()));
							club.setDraws(Integer.valueOf(drawsTextField.getText()));
							club.setPoints(Integer.valueOf(pointsTextField.getText()));
							club.setLeague(leagueTextField.getText());
							clubDAO.addClub(club);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}
					}

					break;
				case "Usun":
					if (fullNameTextField.getText().equals("") || stadiumTextField.getText().equals("") || goalsBalanceTextField.getText().equals("")
							|| winsTextField.getText().equals("") || lossesTextField.getText().equals("") || drawsTextField.getText().equals("")
							|| pointsTextField.getText().equals("") || leagueTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							club.setFullName(fullNameTextField.getText());
							club.setStadium(stadiumTextField.getText());
							club.setGoalsBalance(Integer.valueOf(goalsBalanceTextField.getText()));
							club.setWins(Integer.valueOf(winsTextField.getText()));
							club.setLosses(Integer.valueOf(lossesTextField.getText()));
							club.setDraws(Integer.valueOf(drawsTextField.getText()));
							club.setPoints(Integer.valueOf(pointsTextField.getText()));
							club.setLeague(leagueTextField.getText());
							clubDAO.removeClub(club);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}
					}
					break;
				default:
					if (fullNameTextField.getText().equals("") || stadiumTextField.getText().equals("") || goalsBalanceTextField.getText().equals("")
							|| winsTextField.getText().equals("") || lossesTextField.getText().equals("") || drawsTextField.getText().equals("")
							|| pointsTextField.getText().equals("") || leagueTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							club.setFullName(fullNameTextField.getText());
							club.setStadium(stadiumTextField.getText());
							club.setGoalsBalance(Integer.valueOf(goalsBalanceTextField.getText()));
							club.setWins(Integer.valueOf(winsTextField.getText()));
							club.setLosses(Integer.valueOf(lossesTextField.getText()));
							club.setDraws(Integer.valueOf(drawsTextField.getText()));
							club.setPoints(Integer.valueOf(pointsTextField.getText()));
							club.setLeague(leagueTextField.getText());
							if (fullNameUpdatedTextField.getText().equals(""))
								fullNameUpdatedTextField.setText(fullNameTextField.getText());
							if (stadiumUpdatedTextField.getText().equals(""))
								stadiumUpdatedTextField.setText(stadiumTextField.getText());
							if (goalsBalanceUpdatedTextField.getText().equals(""))
								goalsBalanceUpdatedTextField.setText(goalsBalanceTextField.getText());
							if (winsUpdatedTextField.getText().equals(""))
								winsUpdatedTextField.setText(winsTextField.getText());
							if (lossesUpdatedTextField.getText().equals(""))
								lossesUpdatedTextField.setText(lossesTextField.getText());
							if (drawsUpdatedTextField.getText().equals(""))
								drawsUpdatedTextField.setText(drawsTextField.getText());
							if (pointsUpdatedTextField.getText().equals(""))
								pointsUpdatedTextField.setText(pointsTextField.getText());
							if (leagueUpdatedTextField.getText().equals(""))
								leagueUpdatedTextField.setText(leagueTextField.getText());


							Club clubUpdated = new Club();
							clubUpdated.setFullName(fullNameUpdatedTextField.getText());
							clubUpdated.setStadium(stadiumUpdatedTextField.getText());
							clubUpdated.setGoalsBalance(Integer.valueOf(goalsBalanceUpdatedTextField.getText()));
							clubUpdated.setWins(Integer.valueOf(winsUpdatedTextField.getText()));
							clubUpdated.setLosses(Integer.valueOf(lossesUpdatedTextField.getText()));
							clubUpdated.setDraws(Integer.valueOf(drawsUpdatedTextField.getText()));
							clubUpdated.setPoints(Integer.valueOf(pointsUpdatedTextField.getText()));
							clubUpdated.setLeague(leagueUpdatedTextField.getText());
							clubDAO.updateClub(club, clubUpdated);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						}
					}
					break;
			}
			this.dispose();
		});
		acceptingButtonPanel.add(commitButton);

		JButton showButton = new JButton("Pokaz kluby");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(commitButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			ArrayList<Club> clubList = null;
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("Pełna Nazwa | Bilans Goli | Wygrane | Przegrane | Remisy | Punkty | Liga \n");
			if (fullNameTextField.getText().equals("") && stadiumTextField.getText().equals("") && goalsBalanceTextField.getText().equals("")
					&& winsTextField.getText().equals("") && lossesTextField.getText().equals("") && drawsTextField.getText().equals("")
					&& pointsTextField.getText().equals("") && leagueTextField.getText().equals("")) {
				clubList = clubDAO.getAllClubs();
			} else {
				try {
					Club club = new Club();
					if (!fullNameTextField.getText().equals(""))
						club.setFullName(fullNameTextField.getText());
					if (!stadiumTextField.getText().equals(""))
						club.setStadium(stadiumTextField.getText());
					if (!goalsBalanceTextField.getText().equals(""))
						club.setGoalsBalance(Integer.valueOf(goalsBalanceTextField.getText()));
					if (!winsTextField.getText().equals(""))
						club.setWins(Integer.valueOf(winsTextField.getText()));
					if (!lossesTextField.getText().equals(""))
						club.setLosses(Integer.valueOf(lossesTextField.getText()));
					if (!drawsTextField.getText().equals(""))
						club.setDraws(Integer.valueOf(drawsTextField.getText()));
					if (!pointsTextField.getText().equals(""))
						club.setPoints(Integer.valueOf(pointsTextField.getText()));
					if (!leagueTextField.getText().equals(""))
						club.setLeague(leagueTextField.getText());
					clubList = clubDAO.getClubs(club);
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				}
			}

			if (clubList != null) {
				for (Club club : clubList) {
					String stringBuilder = (club.getFullName() + " | ") +
							club.getStadium() + " | " + club.getGoalsBalance() + " | " + club.getWins() + " | " +
							club.getLosses() + " | " + club.getDraws() + " | " + club.getPoints() + " | " +
							club.getLeague() + "\n";
					((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}

	protected void switchButtons(boolean b) {
		fullNameUpdatedTextField.setEditable(b);
		stadiumUpdatedTextField.setEditable(b);
		goalsBalanceUpdatedTextField.setEditable(b);
		winsUpdatedTextField.setEditable(b);
		lossesUpdatedTextField.setEditable(b);
		stadiumUpdatedTextField.setEditable(b);
		pointsUpdatedTextField.setEditable(b);
		drawsUpdatedTextField.setEditable(b);
		leagueUpdatedTextField.setEditable(b);
	}
}
