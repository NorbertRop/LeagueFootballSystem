package View;

import DAO.FootballerDAO;
import DAO.FootballerDAOImp;
import Entities.Footballer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.util.ArrayList;

public class FootballerDialog extends MyDialog {
	private FootballerDAO footballerDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField nameTextField;
	private JTextField surnameTextField;
	private JTextField clubTextField;
	private JTextField goalsTextField;
	private JTextField assistsTextField;
	private JTextField birthdayTextField;
	private JTextField salaryTextField;
	private JTextField positionTextField;
	private JTextField heightTextField;
	private JTextField contractTextField;

	private JTextField nameUpdatedTextField;
	private JTextField surnameUpdatedTextField;
	private JTextField clubUpdatedTextField;
	private JTextField goalsUpdatedTextField;
	private JTextField assistsUpdatedTextField;
	private JTextField birthdayUpdatedTextField;
	private JTextField salaryUpdatedTextField;
	private JTextField positionUpdatedTextField;
	private JTextField heightUpdatedTextField;
	private JTextField contractUpdatedTextField;

	public FootballerDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(700, 650);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Zarządzanie piłkarzem");

		footballerDAO = new FootballerDAOImp();

		prepareTextFieldPanel();
		prepareRadioBox();
		prepareAcceptingButtonPanel();

		add(radioBoxPanel);
		add(textFieldsPanel);
		add(acceptingButtonPanel);
		setResizable(false);
	}

	private void prepareTextFieldPanel() {
		nameTextField = new JTextField();
		surnameTextField = new JTextField();
		clubTextField = new JTextField();
		goalsTextField = new JTextField();
		assistsTextField = new JTextField();
		birthdayTextField = new JTextField();
		salaryTextField = new JTextField();
		positionTextField = new JTextField();
		heightTextField = new JTextField();
		contractTextField = new JTextField();

		nameUpdatedTextField = new JTextField();
		nameUpdatedTextField.setEditable(false);
		surnameUpdatedTextField = new JTextField();
		surnameUpdatedTextField.setEditable(false);
		clubUpdatedTextField = new JTextField();
		clubUpdatedTextField.setEditable(false);
		goalsUpdatedTextField = new JTextField();
		goalsUpdatedTextField.setEditable(false);
		assistsUpdatedTextField = new JTextField();
		assistsUpdatedTextField.setEditable(false);
		birthdayUpdatedTextField = new JTextField();
		birthdayUpdatedTextField.setEditable(false);
		salaryUpdatedTextField = new JTextField();
		salaryUpdatedTextField.setEditable(false);
		positionUpdatedTextField = new JTextField();
		positionUpdatedTextField.setEditable(false);
		heightUpdatedTextField = new JTextField();
		heightUpdatedTextField.setEditable(false);
		contractUpdatedTextField = new JTextField();
		contractUpdatedTextField.setEditable(false);

		JLabel nameLabel = new JLabel("Imie");
		JLabel surnameLabel = new JLabel("Nazwisko");
		JLabel clubLabel = new JLabel("Drużyna");
		JLabel goalsLabel = new JLabel("Gole");
		JLabel assistsLabel = new JLabel("Asysty");
		JLabel birthdayLabel = new JLabel("Urodziny");
		JLabel salaryLabel = new JLabel("Zarobki");
		JLabel positionLabel = new JLabel("Pozycja");
		JLabel heightLabel = new JLabel("Wysokość");
		JLabel contractLabel = new JLabel("Data kontraktu");


		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(10, 3, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(600, 400));
		textFieldsPanel.add(nameLabel);
		textFieldsPanel.add(nameTextField);
		textFieldsPanel.add(nameUpdatedTextField);

		textFieldsPanel.add(surnameLabel);
		textFieldsPanel.add(surnameTextField);
		textFieldsPanel.add(surnameUpdatedTextField);

		textFieldsPanel.add(clubLabel);
		textFieldsPanel.add(clubTextField);
		textFieldsPanel.add(clubUpdatedTextField);

		textFieldsPanel.add(goalsLabel);
		textFieldsPanel.add(goalsTextField);
		textFieldsPanel.add(goalsUpdatedTextField);

		textFieldsPanel.add(assistsLabel);
		textFieldsPanel.add(assistsTextField);
		textFieldsPanel.add(assistsUpdatedTextField);

		textFieldsPanel.add(birthdayLabel);
		textFieldsPanel.add(birthdayTextField);
		textFieldsPanel.add(birthdayUpdatedTextField);

		textFieldsPanel.add(salaryLabel);
		textFieldsPanel.add(salaryTextField);
		textFieldsPanel.add(salaryUpdatedTextField);

		textFieldsPanel.add(positionLabel);
		textFieldsPanel.add(positionTextField);
		textFieldsPanel.add(positionUpdatedTextField);

		textFieldsPanel.add(heightLabel);
		textFieldsPanel.add(heightTextField);
		textFieldsPanel.add(heightUpdatedTextField);

		textFieldsPanel.add(contractLabel);
		textFieldsPanel.add(contractTextField);
		textFieldsPanel.add(contractUpdatedTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(500, 80));
		JButton commitButton = new JButton("Zatwierdz");
		commitButton.setPreferredSize(new Dimension(200, 50));
		commitButton.setFont(commitButton.getFont().deriveFont(20.0f));
		commitButton.addActionListener(event -> {
			Footballer footballer = new Footballer();

			switch (buttonGroup.getSelection().getActionCommand()) {
				case "Dodaj":
					if (nameTextField.getText().equals("") || surnameTextField.getText().equals("") || birthdayTextField.getText().equals("") || heightTextField.getText().equals("") || positionTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Błąd", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							footballer.setName(nameTextField.getText());
							footballer.setSurname(surnameTextField.getText());
							if (!clubTextField.getText().equals(""))
								footballer.setClub(clubTextField.getText());
							if (!goalsTextField.getText().equals(""))
								footballer.setGoals(Integer.valueOf(goalsTextField.getText()));
							if (!assistsTextField.getText().equals(""))
								footballer.setAssists(Integer.valueOf(assistsTextField.getText()));
							footballer.setBirthday(Date.valueOf(birthdayTextField.getText()));
							if (!salaryTextField.getText().equals(""))
								footballer.setSalary(Integer.valueOf(salaryTextField.getText()));
							footballer.setPosition(positionTextField.getText());
							footballer.setHeight(Integer.parseInt(heightTextField.getText()));
							if (!contractTextField.getText().equals("")) {
								footballer.setContract(Date.valueOf(contractTextField.getText()));
							}
							footballerDAO.addFootballer(footballer);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						} catch (IllegalArgumentException e) {
							JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
						}
					}

					break;
				case "Usun":
					if (nameTextField.getText().equals("") || surnameTextField.getText().equals("") || birthdayTextField.getText().equals("") || heightTextField.getText().equals("") || positionTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							footballer.setName(nameTextField.getText());
							footballer.setSurname(surnameTextField.getText());
							if (!clubTextField.getText().equals(""))
								footballer.setClub(clubTextField.getText());
							if (!goalsTextField.getText().equals(""))
								footballer.setGoals(Integer.valueOf(goalsTextField.getText()));
							if (!assistsTextField.getText().equals(""))
								footballer.setAssists(Integer.valueOf(assistsTextField.getText()));
							footballer.setBirthday(Date.valueOf(birthdayTextField.getText()));
							if (!salaryTextField.getText().equals(""))
								footballer.setSalary(Integer.valueOf(salaryTextField.getText()));
							footballer.setPosition(positionTextField.getText());
							footballer.setHeight(Integer.parseInt(heightTextField.getText()));
							if (!contractTextField.getText().equals("")) {
								footballer.setContract(Date.valueOf(contractTextField.getText()));
							}
							footballerDAO.removeFootballer(footballer);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						} catch (IllegalArgumentException e) {
							JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
						}
					}
					break;
				default:
					if (nameTextField.getText().equals("") || surnameTextField.getText().equals("") || birthdayTextField.getText().equals("") || heightTextField.getText().equals("") || positionTextField.getText().equals("")) {
						JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
					} else {
						try {
							footballer.setName(nameTextField.getText());
							footballer.setSurname(surnameTextField.getText());
							footballer.setBirthday(Date.valueOf(birthdayTextField.getText()));
							footballer.setPosition(positionTextField.getText());
							footballer.setHeight(Integer.parseInt(heightTextField.getText()));
							if (nameUpdatedTextField.getText().equals(""))
								nameUpdatedTextField.setText(nameTextField.getText());
							if (surnameUpdatedTextField.getText().equals(""))
								surnameUpdatedTextField.setText(surnameTextField.getText());
							if (clubUpdatedTextField.getText().equals(""))
								clubUpdatedTextField.setText(clubTextField.getText());
							if (goalsUpdatedTextField.getText().equals(""))
								goalsUpdatedTextField.setText(goalsTextField.getText());
							if (assistsUpdatedTextField.getText().equals(""))
								assistsUpdatedTextField.setText(assistsTextField.getText());
							if (birthdayUpdatedTextField.getText().equals(""))
								birthdayUpdatedTextField.setText(birthdayTextField.getText());
							if (salaryUpdatedTextField.getText().equals(""))
								salaryUpdatedTextField.setText(salaryTextField.getText());
							if (positionUpdatedTextField.getText().equals(""))
								positionUpdatedTextField.setText(positionTextField.getText());
							if (heightUpdatedTextField.getText().equals(""))
								heightUpdatedTextField.setText(heightTextField.getText());
							if (contractUpdatedTextField.getText().equals(""))
								contractUpdatedTextField.setText(contractTextField.getText());

							Footballer footballerUpdated = new Footballer();
							footballerUpdated.setName(nameUpdatedTextField.getText());
							footballerUpdated.setSurname(surnameUpdatedTextField.getText());
							if (!clubUpdatedTextField.getText().equals(""))
								footballerUpdated.setClub(clubUpdatedTextField.getText());
							if (!goalsUpdatedTextField.getText().equals(""))
								footballerUpdated.setGoals(Integer.valueOf(goalsUpdatedTextField.getText()));
							if (!assistsUpdatedTextField.getText().equals(""))
								footballerUpdated.setAssists(Integer.valueOf(assistsUpdatedTextField.getText()));
							footballerUpdated.setBirthday(Date.valueOf(birthdayUpdatedTextField.getText()));
							if (!salaryUpdatedTextField.getText().equals(""))
								footballerUpdated.setSalary(Integer.valueOf(salaryUpdatedTextField.getText()));
							footballerUpdated.setPosition(positionUpdatedTextField.getText());
							footballerUpdated.setHeight(Integer.parseInt(heightUpdatedTextField.getText()));
							if (!contractUpdatedTextField.getText().equals("")) {
								footballer.setContract(Date.valueOf(contractTextField.getText()));
							}
							footballerDAO.updateFootballer(footballer, footballerUpdated);
						} catch (NumberFormatException e) {
							JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
						} catch (IllegalArgumentException e) {
							JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
						}
					}
					break;
			}
			this.dispose();
		});
		acceptingButtonPanel.add(commitButton);

		JButton showButton = new JButton("Pokaz piłkarzy");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(commitButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			ArrayList<Footballer> footballersList;
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("ID | Imie | Nazwisko | Klub | Gole | Asysty | Data Urodzenia | Pensja | Pozycja | Wysokość | Kontrakt\n");
			if (nameTextField.getText().equals("") && surnameTextField.getText().equals("") && goalsTextField.getText().equals("") && assistsTextField.getText().equals("")
					&& birthdayTextField.getText().equals("") && heightTextField.getText().equals("") && positionTextField.getText().equals("") && contractTextField.getText().equals("")) {
				footballersList = footballerDAO.getAllFootballers();
			} else {
				Footballer footballer = new Footballer();
				try {
					if (!nameTextField.getText().equals(""))
						footballer.setName(nameTextField.getText());
					if (!surnameTextField.getText().equals(""))
						footballer.setSurname(surnameTextField.getText());
					if (!clubTextField.getText().equals(""))
						footballer.setClub(clubTextField.getText());
					if (!goalsTextField.getText().equals(""))
						footballer.setGoals(Integer.valueOf(goalsTextField.getText()));
					if (!assistsTextField.getText().equals(""))
						footballer.setAssists(Integer.valueOf(assistsTextField.getText()));
					if (!birthdayTextField.getText().equals(""))
						footballer.setBirthday(Date.valueOf(birthdayTextField.getText()));
					if (!salaryTextField.getText().equals(""))
						footballer.setSalary(Integer.valueOf(salaryTextField.getText()));
					if (!positionTextField.getText().equals(""))
						footballer.setPosition(positionTextField.getText());
					if (!heightTextField.getText().equals(""))
						footballer.setHeight(Integer.valueOf(heightTextField.getText()));
					if (!contractTextField.getText().equals("")) {
						footballer.setContract(Date.valueOf(contractTextField.getText()));
					}
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
				}
				footballersList = footballerDAO.getFootballers(footballer);
			}

			if (footballersList != null) {
				for (Footballer footballer : footballersList) {
					String stringBuilder = footballer.getFootballerID() + " | " + (footballer.getName() + " | ") +
							footballer.getSurname() + " | " +
							footballer.getClub() + " | " +
							footballer.getGoals() + " | " +
							footballer.getAssists() + " | " +
							footballer.getBirthday() + " | " +
							footballer.getSalary() + " | " +
							footballer.getPosition() + " | " +
							footballer.getHeight() + " | " +
							footballer.getContract() + " | " + "\n";
					((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}

	protected void switchButtons(boolean b) {
		nameUpdatedTextField.setEditable(b);
		surnameUpdatedTextField.setEditable(b);
		clubUpdatedTextField.setEditable(b);
		goalsUpdatedTextField.setEditable(b);
		assistsUpdatedTextField.setEditable(b);
		birthdayUpdatedTextField.setEditable(b);
		salaryUpdatedTextField.setEditable(b);
		positionUpdatedTextField.setEditable(b);
		heightUpdatedTextField.setEditable(b);
		contractUpdatedTextField.setEditable(b);
	}
}
