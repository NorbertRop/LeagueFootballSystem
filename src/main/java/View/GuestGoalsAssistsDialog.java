package View;

import DAO.GoalAssistDAO;
import DAO.GoalAssistDAOImp;
import Entities.GoalAssist;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class GuestGoalsAssistsDialog extends JDialog{
	private GoalAssistDAO goalAssistDAO;
	private JTextField footballerTextField;
	private JTextField typeTextField;
	private JTextField matchTextField;

	public GuestGoalsAssistsDialog(JDialog owner){
		super(owner);
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setSize(500, 250);
		setLocation(400, 400);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Zarządzanie golami/asystami");
		setLayout(new GridLayout(4, 2, 10, 10));
		goalAssistDAO = new GoalAssistDAOImp();

		footballerTextField = new JTextField();
		typeTextField = new JTextField();
		matchTextField = new JTextField();

		JLabel footballerLabel = new JLabel("ID Piłkarza");
		JLabel typeLabel = new JLabel("Wpisz 'gol/asysta'");
		JLabel matchLabel = new JLabel("ID Meczu");
		JButton showB = new JButton("Pokaż gole/asysty");
		showB.addActionListener(e ->{
			GoalAssist ga = new GoalAssist();
			try{
				if(!footballerTextField.getText().equals("")) {
					ga.setFootballer(Integer.valueOf(footballerTextField.getText()));
				}
				if(typeTextField.getText().equals("gol") || typeTextField.getText().equals("asysta")){
					ga.setType(typeTextField.getText());
				} else {
					ga.setType(null);
				}
				if(!matchTextField.getText().equals("")) {
					ga.setMatch(Integer.valueOf(matchTextField.getText()));
				}
				ArrayList<GoalAssist> goalAssists = goalAssistDAO.getGoals(ga);
				((GuestMatchDialog)owner).getDisplay().setText("Piłkarz | Typ | Match \n");
				for (GoalAssist g : goalAssists) {
					String stringBuilder = g.getFootballer() + " | " + g.getType() + " | " +
							g.getMatch() + "\n";
					((GuestMatchDialog)owner).getDisplay().setText(((GuestMatchDialog)owner).getDisplay().getText() + stringBuilder);
				}
			} catch(NumberFormatException e1){
				JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
			this.dispose();
		});
		add(footballerLabel);
		add(footballerTextField);
		add(typeLabel);
		add(typeTextField);
		add(matchLabel);
		add(matchTextField);
		add(showB);
	}
}
