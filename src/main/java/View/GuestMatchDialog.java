package View;

import DAO.MatchDAO;
import DAO.MatchDAOImp;
import Entities.Match;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.util.ArrayList;

public class GuestMatchDialog extends JDialog {
	private MatchDAO matchDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextArea display;
	private JScrollPane scrollPane;
	private JTextField team1TextField;
	private JTextField team2TextField;
	private JTextField homeTeamTextField;
	private JTextField matchTimeTextField;
	private JTextField leagueTextField;
	private JTextField shootsTeam1TextField;
	private JTextField shootsTeam2TeamTextField;
	private JTextField possessionTeam1TextField;
	private JTextField possessionTeam2TextField;
	private JTextField POTMTextField;
	private JTextField roundTextField;

	public GuestMatchDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(1450, 800);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Zarządzanie meczem");

		matchDAO = new MatchDAOImp();

		prepareTextFieldPanel();
		prepareAcceptingButtonPanel();

		display = new JTextArea();
		display.setEditable(false);
		display.setFont(display.getFont().deriveFont(24.0f));
		scrollPane = new JScrollPane(display);
		scrollPane.setPreferredSize(new Dimension(new Dimension(500, 700)));
		add(scrollPane, BorderLayout.LINE_END);
		add(textFieldsPanel, BorderLayout.CENTER);
		add(acceptingButtonPanel, BorderLayout.PAGE_END);
	}

	private void prepareTextFieldPanel() {
		team1TextField = new JTextField();
		team2TextField = new JTextField();
		homeTeamTextField = new JTextField();
		matchTimeTextField = new JTextField();
		leagueTextField = new JTextField();
		shootsTeam1TextField = new JTextField();
		shootsTeam2TeamTextField = new JTextField();
		possessionTeam1TextField = new JTextField();
		possessionTeam2TextField = new JTextField();
		POTMTextField = new JTextField();
		roundTextField = new JTextField();

		JLabel team1Label = new JLabel("Drużyna 1");
		JLabel team2Label = new JLabel("Drużyna 2");
		JLabel homeTeamLabel = new JLabel("Gospodarz");
		JLabel matchTimeLabel = new JLabel("Data meczu");
		JLabel leagueLabel = new JLabel("Liga");
		JLabel shootsTeam1Label = new JLabel("Strzały Drużyny 1");
		JLabel shootsTeam2Label = new JLabel("Strzały Drużyny 2");
		JLabel possessionTeam1Label = new JLabel("Posiadanie piłki 1");
		JLabel possessionTeam2Label = new JLabel("Posiadanie piłki 2");
		JLabel POTMLabel = new JLabel("POTM");
		JLabel roundLabel = new JLabel("Round");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(11, 2, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(400, 600));

		textFieldsPanel.add(team1Label);
		textFieldsPanel.add(team1TextField);

		textFieldsPanel.add(team2Label);
		textFieldsPanel.add(team2TextField);

		textFieldsPanel.add(homeTeamLabel);
		textFieldsPanel.add(homeTeamTextField);

		textFieldsPanel.add(matchTimeLabel);
		textFieldsPanel.add(matchTimeTextField);

		textFieldsPanel.add(leagueLabel);
		textFieldsPanel.add(leagueTextField);

		textFieldsPanel.add(shootsTeam1Label);
		textFieldsPanel.add(shootsTeam1TextField);

		textFieldsPanel.add(shootsTeam2Label);
		textFieldsPanel.add(shootsTeam2TeamTextField);

		textFieldsPanel.add(possessionTeam1Label);
		textFieldsPanel.add(possessionTeam1TextField);

		textFieldsPanel.add(possessionTeam2Label);
		textFieldsPanel.add(possessionTeam2TextField);

		textFieldsPanel.add(POTMLabel);
		textFieldsPanel.add(POTMTextField);

		textFieldsPanel.add(roundLabel);
		textFieldsPanel.add(roundTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(250, 100));
		JButton showButton = new JButton("Pokaz mecze");
		showButton.setPreferredSize(new Dimension(250, 50));
		showButton.setFont(showButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("ID | Drużyna 1 | Druzyna 2 | Gospodarz | Data meczu | Liga | Strzały Drużyny 1 | Strzały Drużyny 2 | " +
					"Posiadanie Piłki 1 | Posiadanie Piłki 2 | POTM | Runda\n");
			ArrayList<Match> matchList = null;
			Match m = new Match();
			if (team1TextField.getText().equals("") && team2TextField.getText().equals("") && homeTeamTextField.getText().equals("")
					&& matchTimeTextField.getText().equals("") && leagueTextField.getText().equals("") && shootsTeam1TextField.getText().equals("" )
					&& shootsTeam2TeamTextField.getText().equals("" ) && possessionTeam1TextField.getText().equals("") && possessionTeam2TextField.getText().equals("")
					&& POTMTextField.getText().equals("") && roundTextField.getText().equals("")) {
				matchList = matchDAO.getAllMatches();
			} else {
				try {
					if (!team1TextField.equals("")) {
						m.setTeam1(team1TextField.getText());
					}
					if (!team2TextField.getText().equals("")) {
						m.setTeam2(team2TextField.getText());
					}
					if (!homeTeamTextField.getText().equals("")) {
						m.setHomeTeam(homeTeamTextField.getText());
					}
					if (!matchTimeTextField.getText().equals("")) {
						m.setMatchTime(Date.valueOf(matchTimeTextField.getText()));
					}
					if (!leagueTextField.getText().equals("")) {
						m.setLeague(leagueTextField.getText());
					}
					if (!shootsTeam1TextField.getText().equals("")) {
						m.setShootsTeam1(Integer.valueOf(shootsTeam1TextField.getText()));
					}
					if (!shootsTeam2TeamTextField.getText().equals("")) {
						m.setShootsTeam2(Integer.valueOf(shootsTeam2TeamTextField.getText()));
					}
					if (!possessionTeam1TextField.getText().equals("")) {
						m.setPossessionTeam1(Integer.valueOf(possessionTeam1TextField.getText()));
					}
					if (!possessionTeam2TextField.getText().equals("")) {
						m.setPossessionTeam2(Integer.valueOf(possessionTeam2TextField.getText()));
					}
					if (!POTMTextField.getText().equals("")) {
						m.setPOTM(Integer.valueOf(POTMTextField.getText()));
					}
					if (!roundTextField.getText().equals("")) {
						m.setRound(Integer.valueOf(roundTextField.getText()));
					}
					matchList = matchDAO.getMatches(m);
				} catch (NumberFormatException e ) {
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				} catch (IllegalArgumentException e ){
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				}
			}

			if (matchList != null) {
				for (Match match : matchList) {
					String stringBuilder = match.getMatchID() + " | " + (match.getTeam1() + " | ") +
							match.getTeam2() + " | " + match.getHomeTeam() + " | " + match.getMatchTime() + " | " +
							match.getLeague() + " | " + match.getShootsTeam1() + " | " + match.getShootsTeam2() + " | " +
							match.getPossessionTeam1() + " | " + match.getPossessionTeam2() + " | " + match.getPOTM() + " | " +
							match.getRound() + "\n";
					((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);

		JButton addCardB = new JButton("Pokaż kartki");
		addCardB.addActionListener(e -> {
			GuestCardDialog guestCardDialog = new GuestCardDialog((JDialog) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
			guestCardDialog.setVisible(true);
		});
		addCardB.setPreferredSize(new Dimension(250, 50));
		addCardB.setFont(showButton.getFont().deriveFont(20.0f));
		acceptingButtonPanel.add(addCardB);
		JButton addGoalB = new JButton("Pokaż Gole/Asysty");
		addGoalB.addActionListener(e -> {
			GuestGoalsAssistsDialog guestGoalsAssistsDialog = new GuestGoalsAssistsDialog((JDialog) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
			guestGoalsAssistsDialog.setVisible(true);
		});
		addGoalB.setPreferredSize(new Dimension(250, 50));
		addGoalB.setFont(showButton.getFont().deriveFont(20.0f));
		acceptingButtonPanel.add(addGoalB);
		JButton addSquadB = new JButton("Pokaż Składy");
		addSquadB.addActionListener(e -> {
			GuestSquadDialog guestSquadDialog = new GuestSquadDialog((JDialog) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
			guestSquadDialog.setVisible(true);
		});
		addSquadB.setPreferredSize(new Dimension(250, 50));
		addSquadB.setFont(showButton.getFont().deriveFont(20.0f));
		acceptingButtonPanel.add(addSquadB);
	}

	public JTextArea getDisplay() {
		return display;
	}
}
