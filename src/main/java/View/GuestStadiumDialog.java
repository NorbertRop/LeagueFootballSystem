package View;

import DAO.StadiumDAO;
import DAO.StadiumDAOImp;
import Entities.Stadium;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class GuestStadiumDialog extends JDialog {
	private StadiumDAO stadiumDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField nameTextField;
	private JTextField capacityTextField;
	private JTextField cityTextField;

	public GuestStadiumDialog(JFrame owner){
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(400, 250);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Ligi");

		stadiumDAO = new StadiumDAOImp();

		prepareTextFieldPanel();
		prepareAcceptingButtonPanel();

		add(textFieldsPanel);
		add(acceptingButtonPanel);
	}

	private void prepareTextFieldPanel() {
		nameTextField = new JTextField();
		capacityTextField = new JTextField();
		cityTextField = new JTextField();

		JLabel nameLabel = new JLabel("Nazwa");
		JLabel capacityLabel = new JLabel("Pojemność");
		JLabel cityLabel = new JLabel("Miasto");

		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(3, 2, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(300, 150));

		textFieldsPanel.add(nameLabel);
		textFieldsPanel.add(nameTextField);

		textFieldsPanel.add(capacityLabel);
		textFieldsPanel.add(capacityTextField);

		textFieldsPanel.add(cityLabel);
		textFieldsPanel.add(cityTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(200, 150));

		JButton showButton = new JButton("Pokaz stadiony");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(showButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("Nazwa | Pojemność | Miasto \n");
			Stadium s = new Stadium();
			ArrayList<Stadium> stadiumList = null;
			if (nameTextField.getText().equals("") && capacityTextField.getText().equals("") && cityTextField.getText().equals("")) {
				stadiumList = stadiumDAO.getAllStadiums();
			} else {
				try {
					if (!nameTextField.equals("")) {
						s.setName(nameTextField.getText());
					}
					if (!capacityTextField.getText().equals("")) {
						s.setCapacity(Integer.valueOf(capacityTextField.getText()));
					}
					if (!cityTextField.getText().equals("")) {
						s.setCity(cityTextField.getText());
					}
					stadiumList = stadiumDAO.getStadiums(s);
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				}
			}

			if (stadiumList != null) {
				for (Stadium stadium : stadiumList) {
					String stringBuilder = (stadium.getName() + " | ") +
							stadium.getCapacity() + " | " +
							stadium.getCity() + "\n";
					((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}
}
