package View;

import DAO.UserDAO;
import DBUtils.Passwords;
import Entities.User;

import javax.swing.*;
import java.awt.*;

public class UserDialog extends JDialog {
	private JTextField loginField;
	private JPasswordField passwordField;
	private JTextField favTeamField;
	private JTextField typeField;
	private JButton registerB;

	public UserDialog(JFrame owner) {
		super(owner);
		setTitle("Dodaj uzytkownika");
		setModalityType(ModalityType.APPLICATION_MODAL);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new GridLayout(5, 2, 10, 10));
		loginField = new JTextField(15);
		passwordField = new JPasswordField(15);
		favTeamField = new JTextField(15);
		typeField = new JTextField(15);
		registerB = new JButton("Zarejestruj");

		JLabel nickLabel = new JLabel("Nick");
		JLabel passLabel = new JLabel("Hasło");
		JLabel typeLabel = new JLabel("Typ (admin, user)");
		JLabel favLabel = new JLabel("Ulubiona Drużyna");

		add(nickLabel);
		add(loginField);
		add(passLabel);
		add(passwordField);
		add(typeLabel);
		add(typeField);
		add(favLabel);
		add(favTeamField);
		add(registerB);
		setSize(400, 200);
		setLocation(500, 500);
		registerB.addActionListener(e -> {
			UserDAO userDAO = new UserDAO();
			User user = new User();
			if (loginField.getText().equals("") || passwordField.getPassword().length == 0 || favTeamField.getText().equals("") || typeField.getText().equals("")) {
				JOptionPane.showMessageDialog(this, "Dane niekompletne", "Blad", JOptionPane.ERROR_MESSAGE);
			} else {
				byte[] b = Passwords.getNextSalt();
				user.setNick(loginField.getText());
				user.setPass(Passwords.hash(passwordField.getPassword(), b));
				user.setSalt(b);
				user.setType(typeField.getText());
				user.setFavTeam(favTeamField.getText());
				userDAO.addUser(user);
			}
			this.dispose();
		});
	}
}
