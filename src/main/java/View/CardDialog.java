package View;

import DAO.CardDAO;
import DAO.CardDAOImp;
import Entities.Card;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class CardDialog extends JDialog {
	private CardDAO cardDAO;
	private JTextField footballerTextField;
	private JTextField typeTextField;
	private JTextField matchTextField;

	public CardDialog(JDialog owner){
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(500, 250);
		setLocation(400, 400);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Zarządzanie kartkami");
		setLayout(new GridLayout(5, 2, 10, 10));
		cardDAO = new CardDAOImp();

		footballerTextField = new JTextField();
		typeTextField = new JTextField();
		matchTextField = new JTextField();

		JLabel footballerLabel = new JLabel("ID Piłkarza");
		JLabel typeLabel = new JLabel("Wpisz 'zolta/czerwona'");
		JLabel matchLabel = new JLabel("ID Meczu");
		JButton addB = new JButton("Dodaj");
		addB.addActionListener(e -> {
			Card card = new Card();
			try{
				card.setFootballer(Integer.valueOf(footballerTextField.getText()));
				if(typeTextField.getText().equals("zolta") || typeTextField.getText().equals("czerwona")){
					card.setType(typeTextField.getText());
				} else throw new NumberFormatException();
				card.setMatch(Integer.valueOf(matchTextField.getText()));
				cardDAO.addCard(card);
			} catch(NumberFormatException e1){
				JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
			this.dispose();
		});
		JButton removeB = new JButton("Usuń");
		removeB.addActionListener(e ->{
			Card card = new Card();
			try{
				card.setFootballer(Integer.valueOf(footballerTextField.getText()));
				if(typeTextField.getText().equals("zolta") || typeTextField.getText().equals("czerwona")){
					card.setType(typeTextField.getText());
				} else throw new NumberFormatException();
				card.setMatch(Integer.valueOf(matchTextField.getText()));
				cardDAO.removeCard(card);
			} catch(NumberFormatException e1){
				JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
			this.dispose();
		});
		JButton showB = new JButton("Pokaż składy");
		showB.addActionListener(e ->{
			Card card = new Card();
			try{
				if(!footballerTextField.getText().equals("")) {
					card.setFootballer(Integer.valueOf(footballerTextField.getText()));
				}
				if(typeTextField.getText().equals("zolta") || typeTextField.getText().equals("czerwona")){
					card.setType(typeTextField.getText());
				} else {
					card.setType(null);
				}
				if(!matchTextField.getText().equals("")) {
					card.setMatch(Integer.valueOf(matchTextField.getText()));
				}
				ArrayList<Card> cardList = cardDAO.getCards(card);
				((MatchDialog)owner).getDisplay().setText("Piłkarz | Typ | Match \n");
				for (Card c : cardList) {
					String stringBuilder = c.getFootballer() + " | " + c.getType() + " | " +
							c.getMatch() + "\n";
					((MatchDialog)owner).getDisplay().setText(((MatchDialog)owner).getDisplay().getText() + stringBuilder);
				}
			} catch(NumberFormatException e1){
				JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
			this.dispose();
		});
		add(footballerLabel);
		add(footballerTextField);
		add(typeLabel);
		add(typeTextField);
		add(matchLabel);
		add(matchTextField);
		add(addB);
		add(removeB);
		add(showB);
	}
}
