package View;

import DAO.FootballerDAO;
import DAO.FootballerDAOImp;
import Entities.Footballer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.Date;
import java.util.ArrayList;

public class GuestFootballerDialog extends JDialog {
	private FootballerDAO footballerDAO;
	private JPanel textFieldsPanel;
	private JPanel acceptingButtonPanel;
	private JTextField nameTextField;
	private JTextField surnameTextField;
	private JTextField clubTextField;
	private JTextField goalsTextField;
	private JTextField assistsTextField;
	private JTextField birthdayTextField;
	private JTextField salaryTextField;
	private JTextField positionTextField;
	private JTextField heightTextField;
	private JTextField contractTextField;

	public GuestFootballerDialog(JFrame owner) {
		super(owner);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setSize(700, 550);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Zarządzanie piłkarzem");

		footballerDAO = new FootballerDAOImp();

		prepareTextFieldPanel();
		prepareAcceptingButtonPanel();

		add(textFieldsPanel);
		add(acceptingButtonPanel);
		setResizable(false);
	}

	private void prepareTextFieldPanel() {
		nameTextField = new JTextField();
		surnameTextField = new JTextField();
		clubTextField = new JTextField();
		goalsTextField = new JTextField();
		assistsTextField = new JTextField();
		birthdayTextField = new JTextField();
		salaryTextField = new JTextField();
		positionTextField = new JTextField();
		heightTextField = new JTextField();
		contractTextField = new JTextField();

		JLabel nameLabel = new JLabel("Imie");
		JLabel surnameLabel = new JLabel("Nazwisko");
		JLabel clubLabel = new JLabel("Drużyna");
		JLabel goalsLabel = new JLabel("Gole");
		JLabel assistsLabel = new JLabel("Asysty");
		JLabel birthdayLabel = new JLabel("Urodziny");
		JLabel salaryLabel = new JLabel("Zarobki");
		JLabel positionLabel = new JLabel("Pozycja");
		JLabel heightLabel = new JLabel("Wysokość");
		JLabel contractLabel = new JLabel("Data kontraktu");


		textFieldsPanel = new JPanel();
		textFieldsPanel.setLayout(new GridLayout(10, 3, 10, 10));
		textFieldsPanel.setPreferredSize(new Dimension(600, 400));
		textFieldsPanel.add(nameLabel);
		textFieldsPanel.add(nameTextField);

		textFieldsPanel.add(surnameLabel);
		textFieldsPanel.add(surnameTextField);

		textFieldsPanel.add(clubLabel);
		textFieldsPanel.add(clubTextField);

		textFieldsPanel.add(goalsLabel);
		textFieldsPanel.add(goalsTextField);

		textFieldsPanel.add(assistsLabel);
		textFieldsPanel.add(assistsTextField);

		textFieldsPanel.add(birthdayLabel);
		textFieldsPanel.add(birthdayTextField);

		textFieldsPanel.add(salaryLabel);
		textFieldsPanel.add(salaryTextField);

		textFieldsPanel.add(positionLabel);
		textFieldsPanel.add(positionTextField);

		textFieldsPanel.add(heightLabel);
		textFieldsPanel.add(heightTextField);

		textFieldsPanel.add(contractLabel);
		textFieldsPanel.add(contractTextField);
	}

	private void prepareAcceptingButtonPanel() {
		acceptingButtonPanel = new JPanel();
		acceptingButtonPanel.setPreferredSize(new Dimension(500, 80));

		JButton showButton = new JButton("Pokaz piłkarzy");
		showButton.setPreferredSize(new Dimension(200, 50));
		showButton.setFont(showButton.getFont().deriveFont(20.0f));
		showButton.addActionListener((ActionEvent event) -> {
			ArrayList<Footballer> footballersList;
			((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText("ID | Imie | Nazwisko | Klub | Gole | Asysty | Data Urodzenia | Pensja | Pozycja | Wysokość | Kontrakt\n");
			if (nameTextField.getText().equals("") && surnameTextField.getText().equals("") && goalsTextField.getText().equals("") && assistsTextField.getText().equals("")
					&& birthdayTextField.getText().equals("") && heightTextField.getText().equals("") && positionTextField.getText().equals("") && contractTextField.getText().equals("")) {
				footballersList = footballerDAO.getAllFootballers();
			} else {
				Footballer footballer = new Footballer();
				try {
					if (!nameTextField.getText().equals(""))
						footballer.setName(nameTextField.getText());
					if (!surnameTextField.getText().equals(""))
						footballer.setSurname(surnameTextField.getText());
					if (!clubTextField.getText().equals(""))
						footballer.setClub(clubTextField.getText());
					if (!goalsTextField.getText().equals(""))
						footballer.setGoals(Integer.valueOf(goalsTextField.getText()));
					if (!assistsTextField.getText().equals(""))
						footballer.setAssists(Integer.valueOf(assistsTextField.getText()));
					if (!birthdayTextField.getText().equals(""))
						footballer.setBirthday(Date.valueOf(birthdayTextField.getText()));
					if (!salaryTextField.getText().equals(""))
						footballer.setSalary(Integer.valueOf(salaryTextField.getText()));
					if (!positionTextField.getText().equals(""))
						footballer.setPosition(positionTextField.getText());
					if (!heightTextField.getText().equals(""))
						footballer.setHeight(Integer.valueOf(heightTextField.getText()));
					if (!contractTextField.getText().equals("")) {
						footballer.setContract(Date.valueOf(contractTextField.getText()));
					}
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(this, "Złe dane", "Błąd", JOptionPane.ERROR_MESSAGE);
				} catch (IllegalArgumentException e) {
					JOptionPane.showMessageDialog(null, "Złe dane", "Blad", JOptionPane.ERROR_MESSAGE);
				}
				footballersList = footballerDAO.getFootballers(footballer);
			}

			if (footballersList != null) {
				for (Footballer footballer : footballersList) {
					String stringBuilder = footballer.getFootballerID() + " | " + (footballer.getName() + " | ") +
							footballer.getSurname() + " | " +
							footballer.getClub() + " | " +
							footballer.getGoals() + " | " +
							footballer.getAssists() + " | " +
							footballer.getBirthday() + " | " +
							footballer.getSalary() + " | " +
							footballer.getPosition() + " | " +
							footballer.getHeight() + " | " +
							footballer.getContract() + " | " + "\n";
					((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().setText(((ClientView) SwingUtilities.getWindowAncestor(this)).getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
			this.dispose();
		});
		acceptingButtonPanel.add(showButton);
	}
}


