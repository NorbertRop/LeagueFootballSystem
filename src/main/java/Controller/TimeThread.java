package Controller;

import DAO.MatchDAO;
import DAO.MatchDAOImp;
import DBUtils.DBConnection;
import View.MainPanel;

import javax.swing.*;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;

public class TimeThread extends Thread {
	private static final int SECONDS_IN_A_DAY = 24 * 60 * 60;
	private JTextField timer;
	private String favTeam;
	private MatchDAO matchDAO;

	public TimeThread(MainPanel mainPanel, String favTeam) {
		timer = mainPanel.getTimer();
		this.favTeam = favTeam;
		matchDAO = new MatchDAOImp();
	}

	@Override
	public void run() {
		while (true) {
			Calendar thatDay = Calendar.getInstance();
			thatDay.setTime(new Date(0)); /* reset */
			thatDay.set(Calendar.DAY_OF_MONTH, 1);
			thatDay.set(Calendar.MONTH, 0); // 0-11 so 1 less
			thatDay.set(Calendar.YEAR, 2019);
			try {
				if(!DBConnection.getConnection().isClosed() && matchDAO.getNextMatch(favTeam) != null) {
					thatDay.setTime(matchDAO.getNextMatch(favTeam));
					Calendar today = Calendar.getInstance();
					long diff = thatDay.getTimeInMillis() - today.getTimeInMillis();
					long diffSec = diff / 1000;

					long days = diffSec / SECONDS_IN_A_DAY;
					long secondsDay = diffSec % SECONDS_IN_A_DAY;
					long seconds = secondsDay % 60;
					long minutes = (secondsDay / 60) % 60;
					long hours = (secondsDay / 3600); // % 24 not needed

					String time = "Days "+days + " " + hours + ":" + minutes + ":" + seconds;
					timer.setText(time);
				}
			} catch (SQLException e) {
			}
			synchronized (this) {
				try {
					wait(1000);
				} catch (InterruptedException e) {
					System.exit(1);
				}
			}
		}
	}
}
