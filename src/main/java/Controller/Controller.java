package Controller;


import DAO.*;
import DBUtils.Backup;
import DBUtils.DBConnection;
import DBUtils.Passwords;
import Entities.Footballer;
import Entities.Match;
import Entities.User;
import View.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Controller {
	private ClientView clientView;
	private boolean guest;
	private MatchDAO matchDAO;
	private User user;

	public Controller(ClientView view) {
		this.clientView = view;
		matchDAO = new MatchDAOImp();

		clientView.getLoginPanel().getLoginB().addActionListener(new LoginButtonListener());
		clientView.getLoginPanel().getGuestB().addActionListener(e -> {
			guest = true;
			DBConnection.getGuestConnection();
			setGUI(guest);
		});
	}

	class LoginButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			UserDAO userDAO = new UserDAO();
			DBConnection.getConnection();
			char[] pass = clientView.getLoginPanel().getPasswordField().getPassword();
			String nick = clientView.getLoginPanel().getLoginField().getText();

			user = userDAO.loginUser(nick, pass);


			if (user != null) {
				guest = false;
				setGUI(guest);
			} else {
				JOptionPane.showMessageDialog(null, "Nieprawidłowe login lub hasło", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	class FootballerButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (guest) {
				GuestFootballerDialog guestFootballerDialog = new GuestFootballerDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				guestFootballerDialog.setVisible(true);
			} else {
				FootballerDialog footballerDialog = new FootballerDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				footballerDialog.setVisible(true);
			}
		}
	}

	class LeagueButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (guest) {
				GuestLeagueDialog guestLeagueDialog = new GuestLeagueDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				guestLeagueDialog.setVisible(true);
			} else {
				LeagueDialog leagueDialog = new LeagueDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				leagueDialog.setVisible(true);
			}
		}
	}

	class CoachButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (guest) {
				GuestCoachDialog guestCoachDialog = new GuestCoachDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				guestCoachDialog.setVisible(true);
			} else {
				CoachDialog coachDialog = new CoachDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				coachDialog.setVisible(true);
			}
		}
	}

	class ClubButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (guest) {
				GuestClubDialog guestClubDialog = new GuestClubDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				guestClubDialog.setVisible(true);
			} else {
				ClubDialog coachDialog = new ClubDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				coachDialog.setVisible(true);
			}
		}
	}

	class StadiumButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (guest) {
				GuestStadiumDialog guestStadiumDialog = new GuestStadiumDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				guestStadiumDialog.setVisible(true);
			} else {
				StadiumDialog stadiumDialog = new StadiumDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				stadiumDialog.setVisible(true);
			}
		}
	}

	class MatchesButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (guest) {
				GuestMatchDialog guestMatchDialog = new GuestMatchDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				guestMatchDialog.setVisible(true);
			} else {
				MatchDialog matchDialog = new MatchDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e.getSource()));
				matchDialog.setVisible(true);
			}
		}
	}

	private void setGUI(boolean guest) {
		clientView.getContentPane().removeAll();
		clientView.setMainPanel();
		clientView.getMainPanel().getFootballerB().addActionListener(new FootballerButtonListener());
		clientView.getMainPanel().getLeagueB().addActionListener(new LeagueButtonListener());
		clientView.getMainPanel().getCoachB().addActionListener(new CoachButtonListener());
		clientView.getMainPanel().getClubB().addActionListener(new ClubButtonListener());
		clientView.getMainPanel().getStadiumB().addActionListener(new StadiumButtonListener());
		clientView.getMainPanel().getMatchB().addActionListener(new MatchesButtonListener());
		clientView.getMainPanel().getRoundB().addActionListener(e -> {
			clientView.getMainPanel().getDisplay().setText("ID | Drużyna 1 | Druzyna 2 | Gospodarz | Data meczu | Liga | Strzały Drużyny 1 | Strzały Drużyny 2 | " +
					"Posiadanie Piłki 1 | Posiadanie Piłki 2 | POTM | Runda\n");
			try {
				int round = Integer.valueOf(JOptionPane.showInputDialog("Runda"));
				ArrayList<Match> matchArrayList = matchDAO.getRound(round);
				if (matchArrayList != null) {
					for (Match match : matchArrayList) {
						String stringBuilder = match.getMatchID() + " | " + (match.getTeam1() + " | ") +
								match.getTeam2() + " | " + match.getHomeTeam() + " | " + match.getMatchTime() + " | " +
								match.getLeague() + " | " + match.getShootsTeam1() + " | " + match.getShootsTeam2() + " | " +
								match.getPossessionTeam1() + " | " + match.getPossessionTeam2() + " | " + match.getPOTM() + " | " +
								match.getRound() + "\n";
						clientView.getMainPanel().getDisplay().setText(clientView.getMainPanel().getDisplay().getText() + stringBuilder);
					}
				}
			} catch (NumberFormatException e2) {
				JOptionPane.showMessageDialog(clientView, "Zła runda", "Błąd", JOptionPane.ERROR_MESSAGE);
			}
		});
		clientView.getMainPanel().getKingGoalsB().addActionListener(e -> {
			clientView.getMainPanel().getDisplay().setText("Imie | Nazwisko | Klub | Gole\n");
			ArrayList<Footballer> footballersList = (new FootballerDAOImp()).getAssistsKing();

			if (footballersList != null) {
				for (Footballer footballer : footballersList) {
					String stringBuilder = (footballer.getName() + " | ") +
							footballer.getSurname() + " | " +
							footballer.getClub() + " | " +
							footballer.getGoals() + "\n";
					clientView.getMainPanel().getDisplay().setText(clientView.getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
		});
		clientView.getMainPanel().getKingAssists().addActionListener(e -> {
			clientView.getMainPanel().getDisplay().setText("Imie | Nazwisko | Klub | Asysty\n");
			ArrayList<Footballer> footballersList = (new FootballerDAOImp()).getGoalKing();

			if (footballersList != null) {
				for (Footballer footballer : footballersList) {
					String stringBuilder = footballer.getFootballerID() + " | " + (footballer.getName() + " | ") +
							footballer.getSurname() + " | " +
							footballer.getClub() + " | " +
							footballer.getAssists() + " | " + "\n";
					clientView.getMainPanel().getDisplay().setText(clientView.getMainPanel().getDisplay().getText() + stringBuilder);
				}
			}
		});
		if (!guest) {
			clientView.getMainPanel().addAdminButtons(user.getType().equals("admin"), user.getFavTeam());
			TimeThread timeThread = new TimeThread(clientView.getMainPanel(), user.getFavTeam());
			timeThread.start();
			clientView.getMainPanel().getBackupB().addActionListener(e12 -> Backup.Backupdbtosql());
			clientView.getMainPanel().getRestoreB().addActionListener(e1 -> Backup.Restoredbfromsql("backup"));
			clientView.getMainPanel().getAddUserB().addActionListener(e13 -> {
				UserDialog userDialog = new UserDialog((JFrame) SwingUtilities.getWindowAncestor((JButton) e13.getSource()));
				userDialog.setVisible(true);
			});
		}
		clientView.setSize(new Dimension(1250, 800));
	}
}
