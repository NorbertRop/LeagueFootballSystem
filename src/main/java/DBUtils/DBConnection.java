package DBUtils;

import javax.swing.*;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.prefs.Preferences;

public class DBConnection {
	private static Connection connection;

	public static Connection getConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
			JOptionPane.showMessageDialog(null, "Bład przy połączeniu", "Blad", JOptionPane.ERROR_MESSAGE);
		}

		if(connection != null){
			return connection;
		}

		Properties properties = new Properties();
		Preferences preferences = Preferences.userNodeForPackage(DBConnection.class);

		//preferences.put("jdbc.url", "jdbc:mysql://localhost/leaguesystem");
		//preferences.put("db_username", "root");
		//preferences.put("db_password", "test");
		//preferences.put("DBName", "leaguesystem");

		String dbUrl = preferences.get("jdbc.url", null);
		String username = preferences.get("db_username", null);
		String password = preferences.get("db_password", null);
		properties.setProperty("user", username);
		properties.setProperty("password", password);
		properties.setProperty("useSSL", "false");
		properties.setProperty("autoReconnect", "true");

		try {
			connection = DriverManager.getConnection(dbUrl, properties);
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Bład przy połączeniu", "Blad", JOptionPane.ERROR_MESSAGE);
		}

		System.out.println("Connected to database");
		return connection;
	}

	public static Connection getGuestConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
			JOptionPane.showMessageDialog(null, "Bład przy połączeniu", "Blad", JOptionPane.ERROR_MESSAGE);
		}

		if(connection != null){
			return connection;
		}

		Properties properties = new Properties();
		Preferences preferences = Preferences.userNodeForPackage(DBConnection.class);
		//preferences.put("db_guest_username", "guest");
		//preferences.put("db_guest_password", "test");

		String dbUrl = preferences.get("jdbc.url", null);
		String username = preferences.get("db_guest_username", null);
		properties.setProperty("user", username);
		properties.setProperty("useSSL", "false");
		properties.setProperty("autoReconnect", "true");

		try {
			connection = DriverManager.getConnection(dbUrl, properties );
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Bład przy połączeniu", "Blad", JOptionPane.ERROR_MESSAGE);
		}

		System.out.println("Connected to database");
		return connection;
	}

	public static void closeConnection() {
		try {
			if (connection != null && !connection.isClosed()) {
				connection.close();
			}
		}
		catch (SQLException ex) {
		}
	}
}
