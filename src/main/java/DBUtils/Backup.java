package DBUtils;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.util.Arrays;
import java.util.prefs.Preferences;

public class Backup {
	public static void Backupdbtosql() {
		try {
			System.out.println("Backup");
			CodeSource codeSource = Backup.class.getProtectionDomain().getCodeSource();
			File jarFile = new File(codeSource.getLocation().toURI().getPath());
			String jarDir = jarFile.getParentFile().getPath();


			Preferences preferences = Preferences.userNodeForPackage(DBConnection.class);
			String dbName = preferences.get("DBName", null);
			String dbUser = preferences.get("db_username", null);
			String dbPass = preferences.get("db_password", null);

			String folderPath = jarDir + "\\backup";

			//Creating Folder if it does not exist
			File f1 = new File(folderPath);
			f1.mkdir();

			//Creating Path Constraints for backup saving
			String savePath = "\"" + jarDir + "\\backup\\" + "backup.sql\"";

			// Used to create a cmd command
			String executeCmd = "mysqldump --opt --hex-blob --routines -u" + dbUser + " -p" + dbPass + " " + dbName + " -r " + savePath;

			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(executeCmd);
			// any error message?
			StreamGobbler errorGobbler = new
					StreamGobbler(proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new
					StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error?
			int processComplete = proc.waitFor();

			// processComplete=0 if correctly executed, will contain other values if not
			if (processComplete == 0) {
				JOptionPane.showMessageDialog(null, "Successfully backup");
			} else {
				JOptionPane.showMessageDialog(null, "Backup failure");
			}

		} catch (URISyntaxException | IOException | InterruptedException ex) {
			JOptionPane.showMessageDialog(null, "Error at Backuprestore" + ex.getMessage());
		}
	}

	public static void Restoredbfromsql(String s) {
		try {
			CodeSource codeSource = Backup.class.getProtectionDomain().getCodeSource();
			File jarFile = new File(codeSource.getLocation().toURI().getPath());
			String jarDir = jarFile.getParentFile().getPath();

			Preferences preferences = Preferences.userNodeForPackage(DBConnection.class);
			String dbName = preferences.get("DBName", null);
			String dbUser = preferences.get("db_username", null);
			String dbPass = preferences.get("db_password", null);

			String restorePath = jarDir + "\\backup" + "\\" + s;

			String[] cmd = new String[]{"mysql", "-b", "-u" + dbUser, "-p" + dbPass, dbName, "-e", "source " + restorePath + ".sql"};

			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(cmd);
			// any error message?
			StreamGobbler errorGobbler = new
					StreamGobbler(proc.getErrorStream(), "ERROR");

			// any output?
			StreamGobbler outputGobbler = new
					StreamGobbler(proc.getInputStream(), "OUTPUT");

			// kick them off
			errorGobbler.start();
			outputGobbler.start();

			// any error?
			int exitVal = proc.waitFor();

			// processComplete=0 if correctly executed, will contain other values if not
			if (exitVal == 0) {
				JOptionPane.showMessageDialog(null, "Successfully restored from SQL : " + s);
			} else {
				JOptionPane.showMessageDialog(null, "Error at restoring");
			}

		} catch (URISyntaxException | IOException | InterruptedException | HeadlessException ex) {
			JOptionPane.showMessageDialog(null, "Error at Restoredbfromsql" + ex.getMessage());
		}

	}

}

class StreamGobbler extends Thread {
	InputStream is;
	String type;

	StreamGobbler(InputStream is, String type) {
		this.is = is;
		this.type = type;
	}

	public void run() {
		try {
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null)
				System.out.println(type + ">" + line);
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "Bład w Backupie", "Blad", JOptionPane.ERROR_MESSAGE);
		}
	}
}